from carl.envs.noWall import NoWall

env_config = {
    "num_agents": 2,
    "width": 8,
    "height": 4,
    "start_pos": [(1, 1), (6, 1)],
    "goal_pos": [(6, 1), (1, 1)],
    "view_size": [8, 8],
    "encoded_observation": [True, True],
}

env = NoWall(env_config)
env.reset()
print(env)
