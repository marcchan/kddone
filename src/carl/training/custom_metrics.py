"""Example of using RLlib's debug callbacks.

Here we use callbacks to track the average CartPole pole angle magnitude as a
custom metric.
"""

from typing import Dict
from ray.rllib.env import BaseEnv
from ray.rllib.policy import Policy
from ray.rllib.evaluation import MultiAgentEpisode, RolloutWorker
from ray.rllib.agents.callbacks import DefaultCallbacks

from moviepy.editor import VideoClip
from time import strftime
import os


def save_video(images, filename):
    if filename is not None:
        frame = images
        duration = len(frame)

        def make_frame(t):
            return frame[int(t)]

        animation = VideoClip(make_frame, duration=duration)
        animation.write_videofile(filename, fps=1)


class CarlCallbacks(DefaultCallbacks):
    def __init__(self, legacy_callbacks_dict: Dict[str, callable] = None):
        super().__init__(legacy_callbacks_dict)
        self.ep_counter = 0
        self.video_path = "video/"
        self.time_stamp = strftime("%y%m%d_%H%M%S")

    def on_episode_start(
            self,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Dict[str, Policy],
            episode: MultiAgentEpisode,
            **kwargs
    ):
        print(self.ep_counter)
        self.is_eval = worker.policy_config.get("in_evaluation")
        if self.is_eval:
            directory = os.path.dirname(self.video_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            self.frame_imgs = []
            self.frame_imgs.append(worker.env.render(mode=None))

    def on_episode_end(
            self,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Dict[str, Policy],
            episode: MultiAgentEpisode,
            **kwargs
    ):
        is_eval = worker.policy_config.get("in_evaluation")

        if is_eval:
            save_video(
                self.frame_imgs,
                "{}{}_{}_eval_{}.mp4".format(self.video_path, self.time_stamp, str.join("", worker.policies_to_train),
                                             self.ep_counter),
            )
        self.ep_counter += 1

        for index, reward in enumerate(episode.agent_rewards.values()):
            episode.custom_metrics[
                "ep_rewards_per_agent_{}".format(str(index))
            ] = reward

        print(
            "episode {} ended with length {} and rewards per agents {}".format(
                episode.episode_id, episode.length, episode.custom_metrics
            )
        )

    def on_episode_step(
            self,
            worker: RolloutWorker,
            base_env: BaseEnv,
            episode: MultiAgentEpisode,
            **kwargs
    ):
        if self.is_eval:
            self.frame_imgs.append(worker.env.render(mode=None))
            print(len(self.frame_imgs))
