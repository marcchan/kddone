import mlflow
from ray.rllib.agents import Trainer


def log_training_params(trainer: Trainer, tune_params=[]):
    if tune_params:
        tune_config = {k: v for (k, v) in trainer.config.items() if k in tune_params}
        mlflow.log_params(tune_config)
    mlflow.log_param("env", trainer.config["env"])
    mlflow.log_params(trainer.config["env_config"])


def log_ray_training_metrics(metrics: dict, step=None):
    """
    logs metrics of a training step to mlflow.

    Args:
        metrics: output of trainer.train()
        step: step counter

    Returns:

    """
    logged_metrics = flatten_metrics(metrics)
    return mlflow.log_metrics(logged_metrics, step)


def flatten_metrics(metrics):
    """
    Use shallow merge to get contents of custom_metrics as well.
    custom_metrics contents will overwrite any metrics contents with the same key.
    https://stackoverflow.com/questions/38987/how-do-i-merge-two-dictionaries-in-a-single-expression-in-python

    Args:
        metrics: output of trainer.train()

    Returns: (mlflow logging compatible) flattened dict with metrics

    """
    custom_metrics = {
        key: value
        for key, value in metrics["custom_metrics"].items()
        if isinstance(value, (int, float))
    }
    policy_metrics = flatten_policy_metrics(metrics)
    metrics = {
        key: value for key, value in metrics.items() if isinstance(value, (int, float))
    }
    logged_metrics = {**metrics, **custom_metrics, **policy_metrics}
    return logged_metrics


def flatten_policy_metrics(metrics: dict):
    """
    Flattens policy metrics
    Args:
        metrics: output of trainer.train()

    Returns: flat dict

    """
    policy_metric_keys = get_policy_metric_keys(metrics)
    ret = {}
    for key in policy_metric_keys:
        for policy_name, value in metrics[key].items():
            ret[key + "_" + policy_name] = value
    return ret


def get_policy_metric_keys(metrics: dict):
    """
    Args:
        metrics: output of trainer.train()

    Returns: all keys for policy specific metrics

    """
    return [key for key in metrics.keys() if key.startswith("policy")]


def _ray_train(trainer: Trainer, steps=360, with_logging=False):
    for i in range(steps):
        result = trainer.train()
        print(result)
        if with_logging:
            log_ray_training_metrics(result, i)


def train(
    experiment_name: str,
    trainer: Trainer,
    steps=360,
    with_logging=True,
    mlflow_endpoint="http://localhost:5000",
):
    """
    Runs a number of training steps on a given trainer
    Args:
        experiment_name: Name for this exeriment in mlflow
        trainer: Ray Trainer to train
        steps: number of training steps to execute
        with_logging: enable mlflow tracking
        mlflow_endpoint: Endpoint of the mlflow server used for logging

    """
    train_multiple_trainers(
        experiment_name, [trainer], steps, with_logging, mlflow_endpoint
    )


def train_multiple_trainers(
    experiment_name: str,
    trainers: list,
    steps=360,
    with_logging=True,
    mlflow_endpoint="http://localhost:5000",
):
    """
        Runs a number of training steps on all given trainers
        Args:
            experiment_name: Name for this exeriment in mlflow
            trainers: Ray Trainers to train
            steps: number of training steps to execute
            with_logging: enable mlflow tracking
            mlflow_endpoint: Endpoint of the mlflow server used for logging

        """

    if with_logging:
        mlflow.set_tracking_uri(mlflow_endpoint)
        mlflow.set_experiment(experiment_name)

        with mlflow.start_run():
            for trainer in trainers:
                log_training_params(trainer)

                _ray_train(trainer, steps, with_logging=with_logging)
    else:
        for trainer in trainers:
            _ray_train(trainer, steps, with_logging=with_logging)
