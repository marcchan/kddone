import numpy as np
from abc import ABC

from carl.envs.GLOBAL_VAR import VEC_TO_ACT
from carl.agents.utils.Dijkstra import Graph
from carl.agents.utils import move_to_action

from ray.rllib.policy.policy import Policy
from ray.rllib.models.modelv2 import restore_original_dimensions
from ray.rllib.utils.annotations import override
from ray.rllib.agents.trainer_template import build_trainer


class DijkstraPolicy(Policy, ABC):
    """Shortest-Path policy that returns actions that lead to the goal_pos in the least amount of steps."""

    def __init__(self, obs_space, action_space, config):
        self._validate(obs_space, action_space)
        super().__init__(obs_space, action_space, config)
        self.grid_graph = Graph()

    @staticmethod
    def _validate(obs_space, action_space):
        """
        This validates if the gym spaces  we're trying to use are valid.
        :param obs_space:
        :param action_space:
        :return:
        """
        # TODO: Refactor together with observation configurability
        # The code below is here for reference
        # if not hasattr(obs_space, "original_space") or \
        #         not isinstance(obs_space.original_space, Tuple):
        #     raise ValueError("Obs space must be a Tuple, got {}. Use ".format(
        #         obs_space) + "MultiAgentEnv.with_agent_groups() to group related "
        #                      "agents for QMix.")
        # if not isinstance(action_space, Tuple):
        #     raise ValueError(
        #         "Action space must be a Tuple, got {}. ".format(action_space) +
        #         "Use MultiAgentEnv.with_agent_groups() to group related "
        #         "agents for QMix.")
        # if not isinstance(action_space.spaces[0], Discrete):
        #     raise ValueError(
        #         "QMix requires a discrete action space, got {}".format(
        #             action_space.spaces[0]))
        # if len({str(x) for x in obs_space.original_space.spaces}) > 1:
        #     raise ValueError(
        #         "Implementation limitation: observations of grouped agents "
        #         "must be homogeneous, got {}".format(
        #             obs_space.original_space.spaces))
        # if len({str(x) for x in action_space.spaces}) > 1:
        #     raise ValueError(
        #         "Implementation limitation: action space of grouped agents "
        #         "must be homogeneous, got {}".format(action_space.spaces))

    def _unpack_observation(self, obs_batch):
        """Unpacks the observation.
        Example output with two agents:
        OrderedDict([('agent_pos', array([[1., 2.],
                              [2., 2.]])), ('goal_pos', [array([[1., 0., 0., 0., 0., 0., 0., 0.],
                              [1., 0., 0., 0., 0., 0., 0., 0.]]), array([[1., 0., 0., 0., 0., 0., 0., 0.],
                              [1., 0., 0., 0., 0., 0., 0., 0.]])]), ('image', array([[[[2., 5., 0.],
                                [2., 5., 0.],
                                [2., 5., 0.]],

                               [[1., 0., 0.],
                                [1., 0., 0.],
                                [1., 0., 0.]],

                               [[1., 0., 0.],
                                [1., 0., 0.],
                                [1., 0., 0.]]],


                              [[[1., 0., 0.],
                                [1., 0., 0.],
                                [1., 0., 0.]],

                               [[1., 0., 0.],
                                [1., 0., 0.],
                                [1., 0., 0.]],

                               [[1., 0., 0.],
                                [1., 0., 0.],
                                [1., 0., 0.]]]]))]) of type: <class 'collections.OrderedDict'>


        Returns:
            obs (np.ndarray): obs tensor of shape [B, n_agents, obs_size]
                or None if it is not in the batch
        """
        # IT WORKS!!!
        unpacked = restore_original_dimensions(
            np.array(obs_batch), self.observation_space, tensorlib=np
        )

        obs = unpacked
        return obs

    @override(Policy)
    def compute_actions(
        self,
        obs_batch,
        state_batches=None,
        prev_action_batch=None,
        prev_reward_batch=None,
        **kwargs
    ):
        """
        Args:
            obs_batch (Union[List,np.ndarray]): Batch of observations.
            state_batches (Optional[list]): List of RNN state input batches,
                if any.
            prev_action_batch (Optional[List,np.ndarray]): Batch of previous
                action values.
            prev_reward_batch (Optional[List,np.ndarray]): Batch of previous
                rewards.
            info_batch (info): Batch of info objects.
            episodes (list): MultiAgentEpisode for each obs in obs_batch.
                This provides access to all of the internal episode state,
                which may be useful for model-based or multiagent algorithms.
            explore (bool): Whether to pick an exploitation or exploration
                action (default: None -> use self.config["explore"]).
            timestep (int): The current (sampling) time step.
            kwargs: forward compatibility placeholder
        Returns:
            actions (list): batch of output actions, with shape like
                [BATCH_SIZE, ACTION_SHAPE].
            state_outs (list): list of RNN state output batches, if any, with
                shape like [STATE_SIZE, BATCH_SIZE].
            info (dict): dictionary of extra feature batches, if any, with
                shape like {"f1": [BATCH_SIZE, ...], "f2": [BATCH_SIZE, ...]}.
        """
        actions = []
        obs_batch = self._unpack_observation(obs_batch)
        assert (
            len(obs_batch) >= 3
        ), "Got following batch observations: {} of type: {}".format(
            obs_batch, type(obs_batch)
        )
        # Expecting image, agent_pos and goal_pos
        # shape: (num_agents, *minigrid.shape)

        for image, abs_start_pos, abs_goal_pos in zip(
            obs_batch["image"], obs_batch["agent_pos"], obs_batch["goal_pos"]
        ):
            actions.append(self._compute_actions(abs_goal_pos, abs_start_pos, image))

        # Alternatively, a numpy array would work here as well.
        # e.g.: np.array(actions])
        return actions, [], {}

    def _compute_actions(self, abs_goal_pos, abs_start_pos, image):
        """
        Calculates the next action.
        Args:
            abs_goal_pos: Grid relative agent position
            abs_start_pos: Grid relative goal position
            image: agent observation

        Returns: next Action

        """
        start_pos = (image.shape[0] // 2, image.shape[1] // 2)
        goal_pos = tuple(start_pos + np.array(abs_goal_pos) - abs_start_pos)
        goal_pos = tuple(np.asarray(goal_pos, dtype=int))
        self.grid_graph.from_encoded_array(image)
        shortest_path = self.grid_graph.shortest_path(repr(start_pos), repr(goal_pos))
        if shortest_path is None:
            # TODO replace this with something like self.nr_actions
            action = np.random.choice(
                range(len(VEC_TO_ACT))
            )  # Take random action since goal is not in
            # agent view
            print(
                "Current pos: {} and taking random action to {} "
                "next since goal {} is not in view".format(start_pos, action, goal_pos)
            )
            return action
        else:
            shortest_path = list(shortest_path)
            print("Shortest Path: {}".format(shortest_path))
            next_pos = eval(shortest_path.pop())
            action = move_to_action(tuple(start_pos), next_pos)
            print("Current pos: {} and going {} next".format(start_pos, action))
            return action

    @override(Policy)
    def compute_single_action(
        self,
        obs,
        state=None,
        prev_action=None,
        prev_reward=None,
        info=None,
        episode=None,
        clip_actions=False,
        explore=None,
        timestep=None,
        **kwargs
    ):
        """Unbatched version of compute_actions, we can use this for our tests e.g..
            Arguments:
                obs (obj): Single observation.
                state (list): List of RNN state inputs, if any.
                prev_action (obj): Previous action value, if any.
                prev_reward (float): Previous reward, if any.
                info (dict): info object, if any
                episode (MultiAgentEpisode): this provides access to all of the
                    internal episode state, which may be useful for model-based or
                    multi-agent algorithms.
                clip_actions (bool): Should actions be clipped?
                explore (bool): Whether to pick an exploitation or exploration
                    action (default: None -> use self.config["explore"]).
                timestep (int): The current (sampling) time step.
                kwargs: forward compatibility placeholder
            Returns:
                actions (obj): single action
                state_outs (list): list of RNN state outputs, if any
                info (dict): dictionary of extra features, if any
        """
        if not isinstance(
            obs, dict
        ):  # No need to unpack if we get the dict from CarlEnv.step or CarlEnv.reset
            obs = self._unpack_observation(obs)
        else:
            obs = list(
                obs.values()
            ).pop()  # Get observation from passed agent without knowing the index
        # Expecting image, agent_pos and goal_pos
        assert len(obs) >= 3, "Got following batch observations: {} of type: {}".format(
            obs, type(obs)
        )
        image = obs["image"]
        start_pos = obs["agent_pos"]
        goal_pos = obs["goal_pos"]

        action = self._compute_actions(goal_pos, start_pos, image)

        return action, [], {}

    @override(Policy)
    def learn_on_batch(self, samples):
        """No learning. Necessary for API."""
        return {}

    @override(Policy)
    def get_weights(self):
        """No learning. Necessary for API."""
        return {}

    @override(Policy)
    def set_weights(self, weights):
        """No learning. Necessary for API."""
        pass


DijkstraTrainer = build_trainer(name="Dijkstra", default_policy=DijkstraPolicy,)
