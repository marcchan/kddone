from carl.envs import CarlEnv
from carl.agents.DijkstraPolicy import DijkstraPolicy

from moviepy.editor import VideoClip

# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv


test_envs = [
    MultiEmptyEnv,
]


def save_video(images, filename):
    if filename is not None:
        frame = images
        duration = len(frame)

        def make_frame(t):
            return frame[int(t)]

        animation = VideoClip(make_frame, duration=duration)
        animation.write_videofile(filename, fps=1)


env_config = {
    "num_agents": 2,
    "grid_size": 8,
    "start_pos": [(1, 1), (6, 1)],
    "goal_pos": [(6, 6), (1, 6)],
    "view_size": [10] * 2,
    "encoded_observation": [True] * 2,
}

dummy_env = CarlEnv({})  # Our action and observation space definitions don't change atm

dijkstra_policy = DijkstraPolicy(
    obs_space=dummy_env.observation_space,
    action_space=dummy_env.action_space,
    config={},
)

done = [False] * env_config["num_agents"]
for Env in test_envs:
    imgs = []
    counter = 0
    multiMiniGrid = Env(env_config)
    last_state = multiMiniGrid.reset()
    img = multiMiniGrid.render()
    imgs.append(img)
    while not all(done):
        act_dict = {}
        for agent_idx, state in last_state.items():
            if not done[agent_idx]:
                action, _, _ = dijkstra_policy.compute_single_action({0: state})
                act_dict[agent_idx] = action
        next_state, _, done, _ = multiMiniGrid.step(act_dict)
        last_state = next_state
        counter += 1
        img = multiMiniGrid.render()
        imgs.append(img)
    save_video(imgs, "test.mp4")
    print("DONE")
