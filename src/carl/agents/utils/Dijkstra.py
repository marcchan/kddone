import heapq
import sys
import random

import gym_minigrid.minigrid
import numpy as np

from carl.envs import is_traversable

"""
Source:
https://github.com/mburst/dijkstras-algorithm/blob/master/dijkstras.py
https://en.wikipedia.org/wiki/Dijkstra's_algorithm#Using_a_priority_queue
"""


class Graph:
    def __init__(self):
        self.vertices = {}

    def add_vertex(self, name, edges):
        self.vertices[name] = edges

    def shortest_path(self, start: str, finish: str, randomized=True):
        """
        Calculates the shortest path to finish from start if possible.
        Returns None if finish is not a node in the Graph.
        :param start: key for node in vertices dict
        :param finish: key for goal node in vertices dict
        :return: List of closest nodes in reverse order or None if finish is not in the Graph. First node to traverse is list.pop()
        """
        distances = {}  # Distance from start to node
        previous = {}  # Previous node in optimal path from source
        nodes = []  # Priority queue of all nodes in Graph

        # Check if finish is in the graph
        if finish not in self.vertices:
            return None
        # We can actually reach finish from start
        else:
            # FIXME This does not add nodes without outgoing vertex
            for vertex in self.vertices:
                if vertex == start:  # Set root node as distance of 0
                    distances[vertex] = 0
                    heapq.heappush(nodes, [0, vertex])
                else:
                    distances[vertex] = sys.maxsize
                    heapq.heappush(nodes, [sys.maxsize, vertex])
                previous[vertex] = None

            while nodes:
                if randomized:
                    smallest_dist = nodes[0][0]
                    alternatives = []
                    for node in heapq.nsmallest(4, nodes):
                        if node[0] <= smallest_dist:
                            alternatives.append(node)
                        else:
                            break

                    smallest = random.choice(alternatives)
                    nodes.remove(smallest)
                    smallest = smallest[1]

                else:
                    smallest = heapq.heappop(nodes)[
                        1
                    ]  # Vertex in nodes with smallest distance in distances
                if (
                    smallest == finish
                ):  # If the closest node is our target we're done so print the path
                    path = []
                    while previous[
                        smallest
                    ]:  # Traverse through nodes til we reach the root which is 0
                        path.append(smallest)
                        smallest = previous[smallest]
                    return path
                if (
                    distances[smallest] == sys.maxsize
                ):  # All remaining vertices are inaccessible from source
                    break

                for neighbor in self.vertices[
                    smallest
                ]:  # Look at all the nodes that this vertex is attached to
                    alt = (
                        distances[smallest] + self.vertices[smallest][neighbor]
                    )  # Alternative path distance
                    if (
                        alt < distances[neighbor]
                    ):  # If there is a new shortest path update our priority queue (relax)
                        distances[neighbor] = alt
                        previous[neighbor] = smallest
                        for n in nodes:
                            if n[1] == neighbor:
                                n[0] = alt
                                break
                heapq.heapify(nodes)
            return distances

    def from_encoded_array(self, encoded_array: np.ndarray):
        """
        Creates graph structure from Minigrid env's internal encoded grid structure.
        :param encoded_array: np.array((height, width, depth))
        :void:
        """
        grid = self.parse_minigrid(encoded_array)
        assert isinstance(
            grid, gym_minigrid.minigrid.Grid
        ), "Got unexpected minigrid object: {} with type: {}".format(grid, type(grid))
        self.from_grid(grid)

    def from_grid(self, grid: gym_minigrid.minigrid.Grid):
        """
        Creates graph structure from Minigrid env's internal grid structure.
        :param grid: gym_minigrid.minigrid.Grid
        :void:
        """
        assert isinstance(
            grid, gym_minigrid.minigrid.Grid
        ), "Got unexpected minigrid object: {} with type: {}".format(grid, type(grid))
        # Each vertex is an object, distances will always be 1 between single grid cells
        minigrid_graph = {}
        for column in range(0, grid.width):
            for row in range(0, grid.height):
                current_cell = grid.get(row, column)
                # Use cell str representation as key and assign neighbors with distance 0
                # Make sure only coordinates are passed. cell types = {None, tuple, WorldObj}
                # None cells are emtpy cells
                if is_traversable(current_cell):
                    minigrid_graph[repr((row, column))] = get_minigrid_neighbor_cells(
                        grid, (row, column)
                    )
        self.vertices = minigrid_graph

    def __str__(self):
        return str(self.vertices)

    @staticmethod
    def parse_minigrid(minigrid):
        """
        Parse possible minigrid objects to valid Grid object.
        :param minigrid: [gym_minigrid.minigrid.Grid, np.array((height, width, depth))]
        :return: gym_minigrid.minigrid.Grid
        """
        try:
            if isinstance(minigrid, gym_minigrid.minigrid.Grid):
                return minigrid
            elif isinstance(minigrid, np.ndarray):
                # shape is (num_agents, *grid.shape)
                # minigrid = minigrid[0]
                assert (
                    len(minigrid.shape) == 3
                ), "Expected encoded Grid to have three dimensions. Got encoded Grid with shape: {}".format(
                    minigrid.shape
                )
                height, width, depth = minigrid.shape
                assert (
                    height == width
                ), "Expected Grid to be symmetric. Got encoded Grid with shape: {}".format(
                    (height, width, depth)
                )
                grid = gym_minigrid.minigrid.Grid(height=height, width=width)
                grid, vis_mask = grid.decode(minigrid)
                # Apply vis_mask one-hot encoded agent view to overall grid

                return grid
        except TypeError:
            print(
                "Got unexpected type for parsing into Grid object: {}".format(
                    type(minigrid)
                )
            )


def get_directions(cell: tuple):
    cell_x, cell_y = cell
    north = (cell_x - 1, cell_y)
    east = (cell_x, cell_y + 1)
    south = (cell_x + 1, cell_y)
    west = (cell_x, cell_y - 1)
    return north, east, south, west


def get_minigrid_neighbor_cells(minigrid: gym_minigrid.minigrid.Grid, cell: tuple):
    assert isinstance(cell, tuple)
    assert len(cell) == 2
    neighbors = {}
    # Make use of __contains__ to avoid index errors in Grid.get()
    # Check all directions for a cell and add existing neighbors to its neighbors object
    directions = get_directions(cell)
    # Valid cells for pathing have the value None
    # These checks work because we have walls keeping us from checking for None and getting nonesense
    for direction in directions:
        try:
            if is_traversable(minigrid.get(*direction)):
                neighbors[repr(direction)] = 1
        except AssertionError:
            pass

    return neighbors
