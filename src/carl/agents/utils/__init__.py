from ast import literal_eval

import numpy as np
from carl.envs.GLOBAL_VAR import VEC_TO_ACT


def path_to_actions(start_position: tuple, shortest_path):
    last_pos = start_position
    actions = []
    for cell_pos in map(literal_eval, shortest_path):
        action = move_to_action(last_pos, cell_pos)
        actions.append(action)
        last_pos = cell_pos
    return actions


def move_to_action(start_position: tuple, next_position: tuple):
    cell_pos = np.array(next_position, dtype=int)
    print(cell_pos.dtype, np.array(start_position).dtype)
    rel = cell_pos - np.array(start_position)
    action = VEC_TO_ACT[str(rel)]
    print("{} - {} = {} ({})".format(start_position, cell_pos, rel, str(action)))
    return action


def act_arr_to_act_dict(act_arr: np.ndarray):
    """
    Transform RLlib actions array into actions dict.
    :param act_arr: actions array with one entry per agent
    :return: dictionary of agents with agent ids as key
    """
    act_dict = {}
    for agent_idx, actions in act_arr:
        act_dict[agent_idx] = actions
    return act_dict
