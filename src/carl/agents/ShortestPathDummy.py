import os
from time import sleep

import numpy as np

from carl import bcolors
from carl.agents.utils import path_to_actions
from carl.agents.utils.Dijkstra import Graph
from carl.envs import CarlEnv
from carl.envs.empty import MultiEmptyEnv

config = {
    "num_agents": 1,
    "view_size": [25],
    "grid_size": 25,
}


def clear():
    """
    Clears the terminal
    """
    _ = os.system("cls" if os.name == "nt" else "clear")


multiMiniGrid = MultiEmptyEnv(config)
multiMiniGrid.reset()
print(multiMiniGrid)

obs, rew, done, info = multiMiniGrid.step({0: CarlEnv.Actions.wait})

image = obs[0]["image"]
graph = Graph()
graph.from_encoded_array(image)
agent = multiMiniGrid.agents[0]
start_pos = (image.shape[0] // 2, image.shape[1] // 2)
goal_pos = tuple(start_pos + np.array(agent.agent_goal_position) - agent.agent_pos)
goal_pos = tuple(np.asarray(goal_pos, dtype=int))

shortest_path = graph.shortest_path(str(tuple(start_pos)), str(goal_pos))
print(shortest_path)
# Shortest path lists node starting from goal...
shortest_path.reverse()
print(shortest_path)
actions = path_to_actions(start_pos, shortest_path)

animate = True
for action in actions:
    action_dict = {}
    for j, agent in zip(range(len(multiMiniGrid.agents)), multiMiniGrid.agents):
        color = bcolors.OKBLUE if j % 2 == 0 else bcolors.OKGREEN
        print(color + str(multiMiniGrid.Actions(action)) + bcolors.ENDC)
        action_dict[j] = action
    ret = multiMiniGrid.step(action_dict)

    if animate:
        sleep(0.5)
        clear()

    print(multiMiniGrid)
    print("")
