"""
 Base class of an autonomously acting and learning agent.
"""
from carl.agents.DijkstraPolicy import DijkstraTrainer

DijkstraTrainer


class Agent:
    def __init__(self, params):
        self.nr_actions = params["nr_actions"]

    """
     Behavioral strategy of the agent. Maps states to actions.
    """

    def policy(self, state):
        raise NotImplementedError

    """
     Learning method of the agent. Integrates experience into
     the agent's current knowledge.
    """

    def update(self, state, action, reward, next_state, done):
        raise NotImplementedError
