from carl import bcolors
from gym import spaces

from carl.envs.empty import MultiEmptyEnv

from moviepy.editor import VideoClip


def save_video(images, filename):
    if filename is not None:
        frame = images
        duration = len(frame)

        def make_frame(t):
            return frame[int(t)]

        animation = VideoClip(make_frame, duration=duration)
        animation.write_videofile(filename, fps=1)


config = {"num_agents": 5, "view_size": [1] * 5}
n_steps = 20
gen_vid = False
multiMiniGrid = MultiEmptyEnv(config)
multiMiniGrid.reset()

imgs = []
for i in range(n_steps):
    img = multiMiniGrid.render()
    imgs.append(img)
    action_dict = {}
    for j, agent in zip(range(len(multiMiniGrid.agents)), multiMiniGrid.agents):
        color = bcolors.OKBLUE if j % 2 == 0 else bcolors.OKGREEN
        action = spaces.Discrete(5).sample()  # multiMiniGrid.action_space.sample()
        print(color + str(multiMiniGrid.Actions(action)) + bcolors.ENDC)
        action_dict[j] = action
    multiMiniGrid.step(action_dict)
    print(multiMiniGrid)
    print("")
if gen_vid:
    save_video(imgs, "test.mp4")
