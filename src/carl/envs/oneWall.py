from gym_minigrid.minigrid import Grid
from ray.tune import register_env

from carl.envs import CarlEnv


class OneWallEnv(CarlEnv):
    def __init__(self, config):
        """
        :param config: num_agents, [start_pos], [goal_pos], [view_size]
        """

        super().__init__(config)
        self.width = config["width"]
        self.height = config["height"]

    def _gen_grid(self, width, height):
        assert height >= 5
        assert width >= 7

        # Create an empty grid
        self.grid = Grid(width, height)

        # Generate the surrounding walls
        self.grid.wall_rect(0, 0, width, height)

        # Generate the one separating walls
        self.grid.horz_wall(2, height // 2, width - 4)

        for agent in self.agents:
            self._place_agent_goal(agent)

            # Place the agent
            agent.agent_pos = agent.agent_start_pos


register_env("OneWallEnv", lambda config: OneWallEnv(config))
