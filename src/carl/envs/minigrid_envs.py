import numpy as np
from gym_minigrid import minigrid
from gym_minigrid.envs import FourRoomsEnv
from gym_minigrid.envs.doorkey import DoorKeyEnv
from gym_minigrid.envs.empty import EmptyEnv
from gym_minigrid.envs.multiroom import MultiRoomEnv

from carl.envs import CarlEnv


def make_multiagent(env: minigrid, minigrid_params=None):
    """
    https://github.com/ray-project/ray/blob/master/rllib/examples/env/multi_agent.py
    Args:
        env: gym-minigrid env class
minigrid_params: config for gym_minigrid environment parametrization
    Returns: MultiAgentEnv

    """

    class MultiEnv(CarlEnv):
        """
        Represents a MultiAgentEnv based on multiple minigrid_envs
        """

        def _gen_grid(self, width, height):
            if minigrid_params is None:
                self.mini_env = env()
            else:
                self.mini_env = env(*minigrid_params)
            self.mini_env._gen_grid(width, height)
            self.grid = self.mini_env.grid

            # Remove goal created by minigrid
            self.grid.grid = [
                None if (cell is not None and cell.type == "goal") else cell
                for cell in self.grid.grid
            ]

            # if config doesn't contain start/goal positions, use start/goal
            # positions from minigrid_env instead of the default values
            if "start_pos" not in self.config:
                for agent in self.agents:
                    agent.agent_start_pos = np.array(self.mini_env.agent_pos)
            if "goal_pos" not in self.config and hasattr(self.mini_env, "goal_pos"):
                for agent in self.agents:
                    agent.agent_goal_position = tuple(self.mini_env.goal_pos)

            # According to minigrid _gen_grid has to set agent_pos
            for agent in self.agents:
                self._place_agent_goal(agent)

                agent.agent_pos = agent.agent_start_pos

        def _gen_agent_obs(self, agent):
            """
            Returns: relative positions of other agents within view extents
            """
            other_agent_pos = self._other_agent_pos(self.agents, agent)

            return list(
                filter(
                    lambda rel_pos: rel_pos is not None,
                    [agent.relative_coords(*pos) for pos in other_agent_pos],
                )
            )

    return MultiEnv


MultiGridEmpty = make_multiagent(EmptyEnv)
MultiDoorKey = make_multiagent(DoorKeyEnv)
MultiGridFourRooms = make_multiagent(FourRoomsEnv)
MultiGridMultiRooms = make_multiagent(MultiRoomEnv, minigrid_params=(4, 4,))
