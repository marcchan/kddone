from gym_minigrid.minigrid import Goal


class AgentGoal(Goal):
    def __init__(self, agents: list):
        """
        This represents an agent specific goal. Environments should use this
        goal to define goals for individual agents. This goal should only be
        observable for the related agents.

        Args:
            agents (list): List of agents which this goal applies to
        """
        super().__init__()
        self.agents = agents

    def is_relevant_for_agent(self, agent):
        return agent in self.agents
