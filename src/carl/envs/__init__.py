from gym import spaces
from gym_minigrid.minigrid import MiniGridEnv, WorldObj
from ray.rllib import MultiAgentEnv

from carl import bcolors
from carl.envs.agent_goal import AgentGoal
from carl.envs.grid import grid_render
from carl.envs.agent_state import AgentState

from enum import IntEnum
import numpy as np

from carl.envs.GLOBAL_VAR import (
    ACT_TO_VEC,
    AGENT_TO_STR,
    OBJECT_TO_STR,
    TILE_PIXELS,
    config_or_default,
)


def is_move_action(action):
    return action in [
        # Maybe discuss if this is intuitive
        CarlEnv.Actions.wait,
        CarlEnv.Actions.north,
        CarlEnv.Actions.east,
        CarlEnv.Actions.south,
        CarlEnv.Actions.west,
    ]


class CarlEnv(MultiAgentEnv):
    class Actions(IntEnum):
        # Do nothing
        wait = 0

        # Move north (up), east (right), south (down), west (left)
        north = 1
        east = 2
        south = 3
        west = 4

        # Pick up an object
        pickup = 5
        # Drop an object
        drop = 6
        # Toggle/activate an object
        toggle = 7

        # Done completing task
        done = 8

    put_obj = MiniGridEnv.put_obj

    def __init__(
        self, config: dict,
    ):
        """
        :param config: num_agents, [start_pos], [goal_pos], [view_size]
        """
        self.config = config

        self.agents = self._gen_agents(config)

        self.grid_size = config_or_default(config, "grid_size")

        self.width = self.grid_size
        self.height = self.grid_size
        self.window = None
        self.carrying = None

        self.max_steps = 4 * self.grid_size * self.grid_size
        self.see_through_walls = config_or_default(config, "see_through_walls")

        self.step_count = 0
        self.grid = None

        # Action enumeration for this environment
        self.actions = CarlEnv.Actions

        # Actions are discrete integer values
        self.action_space = spaces.Discrete(len(self.actions))
        # Observations are dictionaries containing an
        # encoding of the grid and further information on state
        self.observation_space = spaces.Box(
            low=0,
            high=255,
            shape=(
                self.agents[0].agent_view_size * 2 + 1,
                self.agents[0].agent_view_size * 2 + 1,
                3,
            ),
            dtype="uint8",
        )

        observation_dict = {
            "image": self.observation_space,
            "agent_pos": spaces.Box(low=0, high=self.grid_size, shape=(2,)),
            "goal_pos": spaces.Box(low=0, high=self.grid_size, shape=(2,)),
        }

        if config_or_default(config, "other_agents") and len(self.agents) > 1:
            observation_dict["other_agents"] = spaces.Box(
                low=0,
                high=self.grid_size,
                shape=(config_or_default(config, "num_agents") - 1, 2),
            )

        if config_or_default(config, "rel_other_agents") and len(self.agents) > 1:
            observation_dict["rel_other_agents"] = spaces.Box(
                low=-self.grid_size,
                high=self.grid_size,
                shape=(config_or_default(config, "num_agents") - 1, 2),
            )

        self.observation_space = spaces.Dict(observation_dict)

    def reset(self):
        self.step_count = 0

        for agent in self.agents:
            agent.reset()
        self._gen_grid(self.width, self.height)
        assert self.grid is not None
        obs = self.gen_obs()
        return obs

    def gen_obs(self):
        """
        Generate the obeservations for all agents (partially observable,
        low-resolution encoding)
        """
        obs = {}
        for i, agent in enumerate(self.agents):
            obs[i] = self.gen_agent_obs(agent)

        return obs

    def gen_agent_obs(self, agent):
        """
        Generate observation for a single agent
        Args:
            agent: agent for which to generate to observation

        Returns: observation

        """
        grid, vis_mask = self.gen_obs_grid(agent)

        if agent.encoded_observation:
            # Encode the partially observable view into a numpy array
            image = grid.encode(vis_mask)

            if isinstance(agent, AgentState):
                goal_pos = agent.agent_goal_position
            else:
                # TODO change backwards compatible envs to have the correct attributes
                goal_pos = (0, 0)
            # Observations are dictionaries containing:
            # - an image (partially observable view of the environment)
            obs = {
                "image": image,
                "agent_pos": agent.agent_pos,
                "goal_pos": np.asarray(goal_pos),
            }
            if config_or_default(self.config, "other_agents") and len(self.agents) > 1:
                obs["other_agents"] = np.array(
                    self._other_agent_pos(self.agents, agent)
                )

            if (
                config_or_default(self.config, "rel_other_agents")
                and len(self.agents) > 1
            ):
                obs["rel_other_agents"] = np.array(
                    [
                        np.asarray(agent.get_rel_position(*pos))
                        for pos in self._other_agent_pos(self.agents, agent)
                    ]
                )

            return obs
        else:
            obs = {"grid": grid}
            return obs

    def gen_obs_grid(self, agent):
        """
        Generate the sub-grid observed by the agent.
        This method also outputs a visibility mask telling us which grid
        cells the agent can actually see.

        Args:
            agent: agent for which to generate the obs grid
        """

        topX, topY, botX, botY = agent.get_view_exts()

        # create a grid slice to represent the vision range of the agent
        ag_view = 2 * agent.agent_view_size + 1
        grid = self.grid.slice(topX, topY, ag_view, ag_view)

        # Process occluders and visibility
        # Note that this incurs some performance cost
        if not self.see_through_walls:
            # agent is positioned in the middle of the grid slice
            vis_mask = grid.process_vis(
                agent_pos=(agent.agent_view_size // 2, agent.agent_view_size // 2)
            )
        else:
            vis_mask = np.ones(shape=(grid.width, grid.height), dtype=np.bool)

        # Make it so the agent sees what it's carrying
        # We do this by placing the carried object at the agent's position
        # in the agent's partially observable view

        # agent is in the middle of the agent view grid
        agent_pos = grid.width // 2, grid.height // 2

        if agent.carrying:
            grid.set(*agent_pos, self.carrying)
        else:
            grid.set(*agent_pos, None)

        return grid, vis_mask

    def _gen_grid(self, width, height):
        raise NotImplementedError("This should be implemented by separate envs")

    def step(self, action_dict):
        self.step_count += 1
        obs, rew, done, info = {}, {}, {}, {}
        overlap = {}

        # for agent in self.agents:
        #     id = agent.agent_id
        #     obs[id] = self.gen_agent_obs(agent)
        #     rew[id] = 0

        # for agent in self.agents:
        #     id = agent.agent_id
        #     obs[id] = self.gen_agent_obs(agent)
        #     rew[id] = 0

        # for agent in self.agents:
        #     id = agent.agent_id
        #     obs[id] = self.gen_agent_obs(agent)
        #     rew[id] = 0

        for i, action in action_dict.items():
            overlap[i] = self.agent_overlap(self.agents[i])
            obs[i], rew[i], done[i], info[i] = self.agent_step(action, self.agents[i])
            overlap[i] = overlap[i] or self.agent_overlap(self.agents[i])

        for i, over in overlap.items():
            # check all after all steps again
            over = over or self.agent_overlap(self.agents[i])
            if over:
                rew[i] += config_or_default(self.config, "samecell_reward")

        done["__all__"] = (
            len([agent for agent in self.agents if agent.done]) == len(self.agents)
            or self.step_count >= self.max_steps
        )

        return obs, rew, done, info

    def apply_samecell_reward(self, rew, action_dict):
        for i, action in action_dict.items():
            if self.agent_overlap(self.agents[i]):
                rew[i] = config_or_default(self.config, "samecell_reward")

        return rew

    def __action_pos(self, action, agent):
        """
        Calculate new position after applying action
        """
        assert isinstance(action, int) or isinstance(
            action, np.int64
        ), "Got unexpected action: {} of type: {}".format(action, type(action))
        # only able to add moving action
        # from {1, 2, 3, 4}
        if 1 <= action < 5:
            return agent.agent_pos + ACT_TO_VEC[action]
        # position remains unchanged for other actions (wait, done, ect.)
        else:
            return agent.agent_pos

    def agent_overlap(self, agent):
        """
        Check whether the agent stepped into the same cell as another agent
        """
        return (
            len(
                [
                    pos
                    for pos in self._other_agent_pos(self.agents, agent)
                    if all(pos == agent.agent_pos)
                ]
            )
            > 0
        )

    def agent_step(self, action, agent: AgentState):
        """
        Performs given action on the given agent, and returns the reward.
        We handle each action differently. For moving actions we calculate the new position and move the agent.

        :param action: The action to perform
        :param agent: The agent that performs the action
        :return: observation, reward and done status.
        """

        # TODO: Uses one logic branch per action. Doing this in a more functional style might make it less error-prone.

        done = agent.done
        # Get the position in front of the agent
        act_pos = self.__action_pos(action, agent)

        # Get the contents of the cell in front of the agent
        act_cell = self.grid.get(*act_pos)
        # We get this reward for each action
        reward = config_or_default(self.config, "step_reward")
        if is_move_action(action):
            if is_traversable(act_cell):
                agent.agent_pos = act_pos
            if act_cell is not None and act_cell.type == "goal":
                if isinstance(act_cell, AgentGoal):
                    if act_cell.is_relevant_for_agent(agent.agent_id):
                        reward = config_or_default(self.config, "goal_reward")
                        done = True
                else:
                    # Backward compatibility for minigrid Goals
                    reward = config_or_default(self.config, "goal_reward")
                    done = True

        # Pick up an object
        elif action == self.actions.pickup:
            if act_cell and act_cell.can_pickup():
                if agent.carrying is None:
                    agent.carrying = act_cell
                    agent.carrying.cur_pos = np.array([-1, -1])
                    self.grid.set(*act_cell, None)

        # Drop an object
        elif action == self.actions.drop:
            if not act_cell and agent.carrying:
                self.grid.set(*act_cell, self.carrying)
                agent.carrying.cur_pos = act_cell
                agent.carrying = None

        # Toggle/activate an object
        elif action == self.actions.toggle:
            if act_cell:
                act_cell.toggle(self, act_cell)

        # Done action (not used by default)
        elif action == self.actions.done:
            pass

        else:
            assert False, "unknown action: {}".format(action)

        obs = self.gen_agent_obs(agent)

        agent.done = done

        return obs, reward, done, {}

    def _gen_agents(self, config):
        return [AgentState(*args) for args in self._parse_agent_config(config)]

    def _parse_agent_config(self, config):
        """
        Returns an array of tuples, each tuple being the configuration values for one agent.
        Changing this necessitates changes in minigrid_envs.py _gen_agents() as well.
        :param config:
        :return:
        """

        num_agents = config_or_default(config, "num_agents")
        start_pos = config_or_default(config, "start_pos", factor=num_agents)
        goal_pos = config_or_default(config, "goal_pos", factor=num_agents)
        view_size = config_or_default(config, "view_size", factor=num_agents)
        encoded_observation = config_or_default(
            config, "encoded_observation", factor=num_agents
        )

        return zip(
            range(num_agents + 1),
            start_pos[:num_agents],
            goal_pos[:num_agents],
            view_size[:num_agents],
            encoded_observation[:num_agents],
        )

    def __str__(self):
        """
        Adapted from minigrid
        Produce a pretty string of the environment's grid along with the
        agent.
        A grid cell is represented by 2-character string, the first one for
        the object and the second one for the color.
        """

        string = ""

        try:
            for j in range(self.grid.height):

                for i in range(self.grid.width):
                    # Adapted to render all agents this only works for 2
                    # agents
                    agentCount = 0
                    for k, agent in zip(range(len(self.agents)), self.agents):
                        if i == agent.agent_pos[0] and j == agent.agent_pos[1]:

                            if k % 2 == 0:
                                string += bcolors.OKBLUE
                            else:
                                string += bcolors.OKGREEN
                            agentCount += 1
                            string += AGENT_TO_STR
                            string += bcolors.ENDC

                    if agentCount > 0:
                        string += " " * (2 - agentCount)
                        continue

                    c = self.grid.get(i, j)

                    if c is None:
                        string += "  "
                        continue

                    if c.type == "door":
                        if c.is_open:
                            string += "__"
                        elif c.is_locked:
                            string += "L" + c.color[0].upper()
                        else:
                            string += "D" + c.color[0].upper()
                            continue

                    string += OBJECT_TO_STR[c.type] + c.color[0].upper()

                if j < self.grid.height - 1:
                    string += "\n"
        except Exception as e:
            string = str(e)

        return string

    @staticmethod
    def _other_agent_pos(agents, agent):
        """
        Args:
            agent: agent to exclude from agent positions

        Returns: list of agent positions excluding agent

        """
        return [
            other_agent.agent_pos for other_agent in agents if not agent == other_agent
        ]

    def _place_agent_goal(self, agent):
        assert isinstance(agent.agent_goal_position, tuple)
        # Place a goal square for this agent
        goal_cell = self.grid.get(*agent.agent_goal_position)
        if goal_cell is not None:
            if isinstance(goal_cell, AgentGoal):
                # If there is already an AgentGoal, add this agent to it
                goal_cell.agents.append(agent.agent_id)
            else:
                raise ValueError(
                    "Goal for Agent {} can't be placed at {}".format(
                        agent.agent_id, agent.agent_goal_position
                    )
                )
        else:
            self.put_obj(AgentGoal([agent.agent_id]), *agent.agent_goal_position)

    def get_agent_view_exts(self, agent):
        """
        Get view of agent
        :param agent:
        :return:
        """
        # topX, topY, botX, botY = agent.get_view_exts()
        view_size = agent.agent_view_size
        agent_posX, agent_posY = agent.agent_pos

        topX = agent_posX - view_size
        topY = agent_posY - view_size
        botX = agent_posX + view_size
        botY = agent_posY + view_size
        return max(topX, 0), max(topY, 0), min(botX, self.width), min(botY, self.height)

    def agent_view(self, agent):
        topX, topY, botX, botY = agent.get_view_exts()
        return max(topX, 0), max(topY, 0), min(botX, self.width), min(botY, self.height)

    def render(self, mode="human", close=False, highlight=True, tile_size=TILE_PIXELS):
        """
        Render the whole-grid human view
        """
        # TODO: render grid, object, add agent
        if close:
            if self.window:
                self.window.close()
            return

        if mode == "human" and not self.window:
            import gym_minigrid.window

            self.window = gym_minigrid.window.Window("gym_minigrid")
            self.window.show(block=False)

        # Mask of which cells to highlight (scale by overlapping count)
        highlight_mask = np.zeros(shape=(self.width, self.height), dtype=np.int)

        # For each cell in the visibility mask
        for agent in self.agents:
            topX, topY, botX, botY = self.agent_view(agent)
            highlight_mask[topX : botX + 1, topY : botY + 1] += 1

        img = grid_render(
            self.grid,
            tile_size,
            self.agents,
            highlight_mask=highlight_mask if highlight else None,
        )

        if mode == "human":
            self.window.show_img(img)
            # self.window.set_caption(self.mission)
            self.window.set_caption("self.mission")

        return img


def is_traversable(cell):
    """
    Checks if the MiniGrid Cell is traversable, i.e. if the agent is able to walk through it.
    Extend as needed.
    :param cell: grid cell
    :return: bool
    """
    # Empty cells have None as their value
    if cell is None:
        return True
    elif isinstance(cell, WorldObj):
        return cell.can_overlap() or cell.type == "door"
    return False
