from gym_minigrid.minigrid import Grid as minigrid_Grid
from gym_minigrid.rendering import (
    fill_coords,
    point_in_rect,
    highlight_img,
    downsample,
    point_in_triangle,
)

import carl.envs.GLOBAL_VAR as GLOBAL_VAR
import numpy as np


def grid_render(grid, tile_size, agents=None, highlight_mask=None, **kwargs):
    """
    Render this grid at a given scale
    :param r: target renderer object
    :param tile_size: tile size in pixels
    """

    if highlight_mask is None:
        highlight_mask = np.zeros(shape=(grid.width, grid.height), dtype=np.bool)

    # Compute the total grid size
    width_px = grid.width * tile_size
    height_px = grid.height * tile_size

    ag_board = [[[] for i in range(grid.height)] for j in range(grid.width)]
    if agents is not None:
        for agent in agents:
            ag_x, ag_y = agent.agent_pos
            ag_board[ag_x][ag_y].append(agent)

    img = np.zeros(shape=(height_px, width_px, 3), dtype=np.uint8)

    # Render the grid
    for j in range(0, grid.height):
        for i in range(0, grid.width):
            cell = grid.get(i, j)

            tile_img = Grid.render_tile(
                cell,
                agents=ag_board[i][j],
                highlight=highlight_mask[i, j],
                tile_size=tile_size,
            )

            ymin = j * tile_size
            ymax = (j + 1) * tile_size
            xmin = i * tile_size
            xmax = (i + 1) * tile_size
            img[ymin:ymax, xmin:xmax, :] = tile_img

    return img


class Grid(minigrid_Grid):
    """
    Represent a grid and operations on it
    """

    # Static cache of pre-renderer tiles
    tile_cache = {}

    def rotate_left(self):
        pass

    @classmethod
    def render_tile(
        cls,
        obj,
        agents=[],
        highlight=False,
        tile_size=GLOBAL_VAR.TILE_PIXELS,
        subdivs=3,
        **kwargs
    ):
        """
        Render a tile and cache the result
        """

        # Hash map lookup key for the cache
        if len(agents) > 0:
            key = (agents[0].agent_id, len(agents), highlight, tile_size)
        else:
            key = (-1, 0, highlight, tile_size)
        key = obj.encode() + key if obj else key  # cat

        if key in cls.tile_cache:
            return cls.tile_cache[key]

        img = np.zeros(
            shape=(tile_size * subdivs, tile_size * subdivs, 3), dtype=np.uint8
        )

        # Draw the grid lines (top and left edges)
        fill_coords(img, point_in_rect(0, 0.031, 0, 1), (100, 100, 100))
        fill_coords(img, point_in_rect(0, 1, 0, 0.031), (100, 100, 100))

        if len(agents) > 1:
            scale = 0.2 + min((len(agents) - 1) * 0.2, 0.8)
            highlight_img(img, alpha=scale, color=(255, 0, 0))

        # Highlight the cell if needed
        # elif highlight > 0:
        #     scale = 0.1 + min(highlight * 0.15, 0.9)
        #     highlight_img(img, alpha=scale)

        # triangel shape for agent representation
        tri_fn = point_in_triangle((0.81, 0.81), (0.19, 0.81), (0.50, 0.19),)

        if obj is not None:
            obj.render(img)

        # Overlay the agent on top
        if len(agents) > 0:
            # color agent shape (only 9 color in total)
            # only display one (first) agent max
            color = GLOBAL_VAR.AG_COLORS[agents[0].agent_id % 9]
            fill_coords(img, tri_fn, color)

        # Downsample the image to perform supersampling/anti-aliasing
        img = downsample(img, subdivs)

        # Cache the rendered tile
        cls.tile_cache[key] = img

        return img

    def render(self, tile_size, agents=None, highlight_mask=None, **kwargs):
        """
        Render this grid at a given scale
        :param r: target renderer object
        :param tile_size: tile size in pixels
        """

        if highlight_mask is None:
            highlight_mask = np.zeros(shape=(self.width, self.height), dtype=np.bool)

        # Compute the total grid size
        width_px = self.width * tile_size
        height_px = self.height * tile_size

        ag_board = [[[] for i in range(self.width)] for j in range(self.height)]
        if agents is not None:
            for agent in agents:
                ag_x, ag_y = agent.agent_pos
                ag_board[ag_x][ag_y].append(agent)

        img = np.zeros(shape=(height_px, width_px, 3), dtype=np.uint8)

        # Render the grid
        for j in range(0, self.height):
            for i in range(0, self.width):
                cell = self.get(i, j)

                agent_here = ag_board[i][j]
                tile_img = Grid.render_tile(
                    cell,
                    agents=agent_here if len(agent_here) != 0 else None,
                    highlight=highlight_mask[i, j],
                    tile_size=tile_size,
                )

                ymin = j * tile_size
                ymax = (j + 1) * tile_size
                xmin = i * tile_size
                xmax = (i + 1) * tile_size
                img[ymin:ymax, xmin:xmax, :] = tile_img

        return img

    def process_vis(grid, agent_pos):
        mask = np.zeros(shape=(grid.width, grid.height), dtype=np.bool)

        mask[agent_pos[0], agent_pos[1]] = True

        for j in reversed(range(0, grid.height)):
            for i in range(0, grid.width - 1):
                if not mask[i, j]:
                    continue

                cell = grid.get(i, j)
                if cell and not cell.see_behind():
                    continue

                mask[i + 1, j] = True
                if j > 0:
                    mask[i + 1, j - 1] = True
                    mask[i, j - 1] = True

            for i in reversed(range(1, grid.width)):
                if not mask[i, j]:
                    continue

                cell = grid.get(i, j)
                if cell and not cell.see_behind():
                    continue

                mask[i - 1, j] = True
                if j > 0:
                    mask[i - 1, j - 1] = True
                    mask[i, j - 1] = True

        for j in range(0, grid.height):
            for i in range(0, grid.width):
                if not mask[i, j]:
                    grid.set(i, j, None)

        return mask
