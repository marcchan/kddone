import numpy as np


class AgentState:
    """
    This represents the state of one agent in the MultiAgentEnv

    Args:
        agent_id: unique identifier for one agent in the environment
        agent_start_pos: grid cell where the agent is placed on a reset
        agent_goal_pos: position of the goal for the agent
        agent_view_size: range of sight of the agent
        encoded_observation: if the observation for this agent should be
            encoded in a numpy array or the original grid should be returned
    """

    def __init__(
        self,
        agent_id,
        agent_start_pos=(1, 1),
        agent_goal_position=(7, 7),
        agent_view_size=7,
        encoded_observation=True,
    ):
        self.encoded_observation = encoded_observation
        self.agent_view_size = agent_view_size
        self.agent_id = agent_id
        self.agent_goal_position = agent_goal_position
        self.agent_start_pos = np.array(agent_start_pos)
        self.agent_pos = np.array(agent_start_pos)
        self.done = False

        # Carrying functionality is currently not ported from minigrid
        self.carrying = False

    def reset(self):
        self.done = False
        self.carrying = False
        self.agent_pos = self.agent_start_pos

    def get_view_exts(self):
        """
        Get the extents of the square set of tiles visible to the agent
        Note: the bottom extent indices are not included in the set

        (topX, topY)
        +----------------------------+
        |                            |
        |                            |
        |             A              |
        |                            |
        |                            |
        +----------------------------+
                          (botX, botY)

        """

        view_size = self.agent_view_size

        agent_posX, agent_posY = self.agent_pos
        topX = agent_posX - view_size
        topY = agent_posY - view_size
        botX = agent_posX + view_size
        botY = agent_posY + view_size

        return topX, topY, botX, botY

    def get_rel_position(self, x, y):
        """
        Translation vector to (x, y) from agent_pos

        Returns: Tuple

        """
        agent_posX, agent_posY = self.agent_pos
        rel_x = x - agent_posX
        rel_y = y - agent_posY

        return rel_x, rel_y
