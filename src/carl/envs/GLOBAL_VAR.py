import numpy as np


def config_or_default(config, key, factor=None):
    """
    Args:
        config: configuration dict to use
        key: configuration option to get
        factor: options can apply to all agents/the whole environment or to
            individual agents. For options that apply to individual agents the
            number of agents has to be given.

    Returns: Configured value for key from the config or default value

    """
    if factor is None:

        config_value = config.get(key, DEFAULT_CONFIG[key])

        return config_value
    else:
        config_value = config.get(key, DEFAULT_CONFIG[key] * factor)

        if len(config_value) < factor:
            raise ValueError(
                "When setting agent config {}, you have to set "
                "a config for all {} agents!".format(key, factor)
            )

        return config_value


"""
Default Settings for all configuration options
    num_agents: Number of agents in the environment.
    start_pos:  List of start positions for all agents. Positions are Grid
        relative.
    goal_pos:   List of goal positions for all agents. Positions are Grid
        relative.
    grid_size:  Size of the grid in x and y direction by number of tiles.
    view_size:  List of number of tiles each agent can see in every direction.
    encoded_observation:    List for every agent if grid observation should be
        encoded as numpy array or the original Grid View should be given.
    see_through_walls:  If all agents should be able to see through walls
    other_agents:   If Grid relative positions of other agents should be
        included in the observation dict.
    rel_other_agents:   If Observation relative positions of other agents
        should be included in the observation dict.
    goal_reward:    Reward for reaching the goal
    step_reward:    Reward (penalty) for doing one step
    samecell_reward:    Reward (penalty) for stepping into a cell occupied by
        another agent
"""
DEFAULT_CONFIG = {
    "num_agents": 1,
    "start_pos": [(1, 1)],
    "goal_pos": [(6, 6)],
    "grid_size": 8,
    "view_size": [6],
    "encoded_observation": [True],
    "see_through_walls": True,
    "other_agents": True,
    "rel_other_agents": False,
    "goal_reward": 1,
    "step_reward": -0.01,
    "samecell_reward": -0.5,
}

# Size in pixels of a tile in the full-scale human view
TILE_PIXELS = 32

AG_COLORS = [
    [159, 67, 255],  # 0: Purple
    [2, 81, 154],  # 1: Blue
    [204, 0, 204],  # 2: Magenta
    [216, 30, 54],  # 3: Red
    [254, 151, 0],  # 4: Orange
    [100, 255, 255],  # 5: Cyan
    [99, 99, 255],  # 6: Lavender
    [250, 204, 255],  # 7: Pink
    [238, 223, 16],
]  # 8: Yellow

# Map of color names to RGB values
COLORS = {
    "red": [255, 0, 0],
    "green": [0, 255, 0],
    "blue": [0, 0, 255],
    "purple": [112, 39, 195],
    "yellow": [255, 255, 0],
    "grey": [100, 100, 100],
}
COLOR_NAMES = sorted(list(COLORS.keys()))

# Used to map colors to integers
COLOR_TO_IDX = {"red": 0, "green": 1, "blue": 2, "purple": 3, "yellow": 4, "grey": 5}

IDX_TO_COLOR = dict(zip(COLOR_TO_IDX.values(), COLOR_TO_IDX.keys()))

# Map of object type to integers
OBJECT_TO_IDX = {
    "unseen": 0,
    "empty": 1,
    "wall": 2,
    "floor": 3,
    "door": 4,
    "key": 5,
    "ball": 6,
    "box": 7,
    "goal": 8,
    "lava": 9,
    "agent": 10,
}

IDX_TO_OBJECT = dict(zip(OBJECT_TO_IDX.values(), OBJECT_TO_IDX.keys()))

# Map of state names to integers
STATE_TO_IDX = {
    "open": 0,
    "closed": 1,
    "locked": 2,
}

# Map of agent direction indices to vectors
DIR_TO_VEC = [
    # Pointing right (positive X)
    np.array((1, 0)),
    # Down (positive Y)
    np.array((0, 1)),
    # Pointing left (negative X)
    np.array((-1, 0)),
    # Up (negative Y)
    np.array((0, -1)),
]

# Map of object types to short string
OBJECT_TO_STR = {
    "wall": "W",
    "floor": "F",
    "door": "D",
    "key": "K",
    "ball": "A",
    "box": "B",
    "goal": "G",
    "lava": "V",
}

# Short string for opened door
# OPENDED_DOOR_IDS = '_'

# Map agent's direction to short string
AGENT_TO_STR = "x"

ACT_TO_VEC = [
    # wait (do nothing)
    np.array((0, 0)),
    # north (Up)
    np.array((0, -1)),
    # east  (Right)
    np.array((1, 0)),
    # south (Down)
    np.array((0, 1)),
    # west  (Left)
    np.array((-1, 0)),
]

VEC_TO_ACT = {str(vec): i for i, vec in enumerate(ACT_TO_VEC)}
