import ray

from carl.envs import oneWall
from carl.agents.DijkstraPolicy import DijkstraTrainer
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

oneWall

env_config = {
    "num_agents": 2,
    "width": 7,
    "height": 5,
    "start_pos": [(1, 3), (5, 3)],
    "goal_pos": [(5, 1), (1, 1)],
    "view_size": [7, 7],
    "encoded_observation": [True, True],
}


def oneWallShortestPath():

    ray.init(ignore_reinit_error=True)

    trainer = DijkstraTrainer(
        env="OneWallEnv",
        config={
            "num_workers": 1,
            "use_pytorch": False,
            "env_config": env_config,
            "callbacks": CarlCallbacks,
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False}
        },
    )

    train(
        "OneWallShortestPathAgent",
        trainer,
        steps=1,
        with_logging=False,
        mlflow_endpoint="http://10.195.1.81:5000",
    )

    ray.shutdown()


if __name__ == "__main__":
    oneWallShortestPath()
