import ray
from ray.rllib.agents.dqn import DQNTrainer
from ray.rllib.agents.dqn.dqn_torch_policy import DQNTorchPolicy

from carl.envs import oneWall, CarlEnv
from carl.agents import DijkstraTrainer
from carl.agents.DijkstraPolicy import DijkstraPolicy
from carl.training import train_multiple_trainers
from carl.training.custom_metrics import CarlCallbacks

oneWall

# see https://github.com/ray-project/ray/blob/master/rllib/examples/
# multi_agent_two_trainers.py for reference


def trainer_setup():

    env_config = {
        "num_agents": 2,
        "width": 7,
        "height": 5,
        "start_pos": [(1, 3), (5, 3)],
        "goal_pos": [(5, 1), (1, 1)],
        "view_size": [3, 3],
        "encoded_observation": [True, True],
        "rel_other_agents": True,
    }

    dummy_env = CarlEnv(env_config)
    obs_space = dummy_env.observation_space
    act_space = dummy_env.action_space

    def policy_mapping_fn(agent_id):
        if agent_id % 2 == 0:
            return "dijkstra_policy"
        else:
            return "dqn_policy"

    # You can also have multiple policies per trainer, but here we just
    # show one each for PPO and DQN.
    policies = {
        "dijkstra_policy": (DijkstraPolicy, obs_space, act_space, {}),
        "dqn_policy": (DQNTorchPolicy, obs_space, act_space, {}),
    }

    dijkstra_trainer = DijkstraTrainer(
        env="OneWallEnv",
        config={
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": policy_mapping_fn,
                "policies_to_train": ["dijkstra_policy"],
            },
            "env_config": env_config,
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "hiddens": 128,
            "use_pytorch": False,
            "callbacks": CarlCallbacks,
        },
    )

    dqn_trainer = DQNTrainer(
        env="OneWallEnv",
        config={
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": policy_mapping_fn,
                "policies_to_train": ["dqn_policy"],
            },
            "env_config": env_config,
            "evaluation_interval": 5,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": False,
            "callbacks": CarlCallbacks
            # "gamma": 0.95, # TODO
            # "n_step": 3, # TODO
        },
    )

    return dijkstra_trainer, dqn_trainer


def oneWallShortestPathAndDQN():

    ray.init(ignore_reinit_error=True)

    dijkstra_trainer, dqn_trainer = trainer_setup()

    train_multiple_trainers(
        "OneWallShortestPathAgent+DQN",
        [dijkstra_trainer, dqn_trainer],
        steps=100,
        mlflow_endpoint="http://10.195.1.81:5000",
    )

    ray.shutdown()


if __name__ == "__main__":
    oneWallShortestPathAndDQN()
