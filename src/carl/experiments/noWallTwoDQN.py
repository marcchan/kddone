import ray
from ray.rllib.agents.dqn import DQNTrainer

from carl.envs import noWall
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks
# see https://github.com/ray-project/ray/blob/master/rllib/examples/
# multi_agent_two_trainers.py for reference

noWall


def trainer_setup():
    env_config = {
        "num_agents": 2,
        "width": 8,
        "height": 4,
        "start_pos": [(1, 1), (6, 1)],
        "goal_pos": [(6, 1), (1, 1)],
        "view_size": [3, 3],
        "encoded_observation": [True, True],
    }

    dqn_trainer = DQNTrainer(
        env="NoWallEnv",
        config={
            "env_config": env_config,
            "evaluation_interval": 5,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": True,
            "callbacks": CarlCallbacks,
        },
    )

    return dqn_trainer


def noWallTwoDQN():
    ray.init(ignore_reinit_error=True)

    dqn_trainer = trainer_setup()

    train("NoWallTwoDQN", dqn_trainer, steps=300)

    ray.shutdown()


if __name__ == '__main__':
    noWallTwoDQN()
