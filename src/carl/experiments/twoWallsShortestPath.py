import ray

from carl.envs import twoWalls
from carl.agents import DijkstraTrainer
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

# This is here to avoid the linter complaining about unused imports
# Environments are registered in the envs and only their string identifier is necessary to use them thereafter
twoWalls

# Setup for environment #4 from evaluation
# https://gitlab.lrz.de/kddone/kddone/uploads/478074278ab62a16133d022bb56c56a0/env4.png
env_4_config = {
    "num_agents": 2,
    "grid_size": 7,
    "start_pos": [(1, 3), (5, 3)],
    "goal_pos": [(5, 5), (1, 1)],
    "view_size": [7, 7],  # large observation for Dijkstra to see the goal
    "rel_other_agents": True,
    "encoded_observation": [True, True],
}
# Setup for environment #2 with different goal positions
# https://gitlab.lrz.de/kddone/kddone/uploads/38078ccd5ff838be9c0d9043060c888d/env2.png
env_2_config = env_4_config
env_2_config["goal_pos"] = [(5, 3), (1, 3)]


def twoWallsShortestPath():

    ray.init(ignore_reinit_error=True)

    trainer = DijkstraTrainer(
        env="twoWallsEnv",
        config={
            "num_workers": 1,
            "use_pytorch": False,
            "env_config": env_2_config,
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "callbacks": CarlCallbacks,
        },
    )

    train(
        "TwoWallsShortestPathAgent",
        trainer,
        steps=1,
    )

    ray.shutdown()


if __name__ == "__main__":
    twoWallsShortestPath()
