import ray
from carl.agents.DijkstraPolicy import DijkstraTrainer
from carl.envs.noWall import NoWallEnv
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

NoWallEnv

env_config = {
    "num_agents": 2,
    "width": 8,
    "height": 4,
    "start_pos": [(1, 1), (6, 1)],
    "goal_pos": [(6, 1), (1, 1)],
    "view_size": [8, 8],
    "encoded_observation": [True, True],
}


def noWallEnvShortestPath():

    ray.init(ignore_reinit_error=True)
    trainer = DijkstraTrainer(
        env="NoWallEnv",
        config={
            "num_workers": 1,
            "use_pytorch": False,
            "env_config": env_config,
            "callbacks": CarlCallbacks,
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
        },
    )
    train("NoWallShortestPathAgent", trainer, steps=1)
    ray.shutdown()


if __name__ == '__main__':
    noWallEnvShortestPath()
