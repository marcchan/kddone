import ray
from ray.rllib.agents.dqn import DQNTrainer
from ray.rllib.agents.dqn.dqn_torch_policy import DQNTorchPolicy

from carl.envs import twoWalls, CarlEnv
from carl.agents import DijkstraTrainer
from carl.agents.DijkstraPolicy import DijkstraPolicy
from carl.training import train_multiple_trainers
from carl.training.custom_metrics import CarlCallbacks

# This is here to avoid the linter complaining about unused imports
# Environments are registered in the envs and only their string identifier is necessary to use them thereafter
twoWalls

# see https://github.com/ray-project/ray/blob/master/rllib/examples/
# multi_agent_two_trainers.py for reference


def trainer_setup():
    # Setup for environment #4 from evaluation
    # https://gitlab.lrz.de/kddone/kddone/uploads/478074278ab62a16133d022bb56c56a0/env4.png
    env_4_config = {
        "num_agents": 2,
        "grid_size": 7,
        "start_pos": [(1, 3), (5, 3)],
        "goal_pos": [(5, 5), (1, 1)],
        "view_size": [
            7,
            7,
        ],  # Large obs space for Dijkstra, smaller for DQN so that its training converges
        "rel_other_agents": True,
        "encoded_observation": [True, True],
    }
    # Setup for environment #2 with different goal positions
    # https://gitlab.lrz.de/kddone/kddone/uploads/38078ccd5ff838be9c0d9043060c888d/env2.png
    env_2_config = env_4_config
    env_2_config["goal_pos"] = [(5, 3), (1, 3)]
    env_2_config["view_size"] = [4, 4]

    dummy_env = CarlEnv(env_2_config)
    obs_space = dummy_env.observation_space
    act_space = dummy_env.action_space

    def policy_mapping_fn(agent_id):
        if agent_id % 2 == 0:
            return "dijkstra_policy"
        else:
            return "dqn_policy"

    # You can also have multiple policies per trainer, but here we just
    # show one each for PPO and DQN.
    policies = {
        "dijkstra_policy": (DijkstraPolicy, obs_space, act_space, {}),
        "dqn_policy": (DQNTorchPolicy, obs_space, act_space, {}),
    }

    dijkstra_trainer = DijkstraTrainer(
        env="twoWallsEnv",
        config={
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": policy_mapping_fn,
                "policies_to_train": ["dijkstra_policy"],
            },
            "num_workers": 1,
            "use_pytorch": False,
            "env_config": env_2_config,
            "callbacks": CarlCallbacks,
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
        },
    )

    dqn_trainer = DQNTrainer(
        env="twoWallsEnv",
        config={
            "multiagent": {
                "policies": policies,
                "policy_mapping_fn": policy_mapping_fn,
                "policies_to_train": ["dqn_policy"],
            },
            "env_config": env_2_config,
            "evaluation_interval": 100,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": True,
            "callbacks": CarlCallbacks,
        },
    )

    return dijkstra_trainer, dqn_trainer


def twoWallsShortestPathAndDQN():

    ray.init(ignore_reinit_error=True)

    dijkstra_trainer, dqn_trainer = trainer_setup()

    train_multiple_trainers(
        "TwoWallsShortestPathAgent+DQN",
        [dijkstra_trainer, dqn_trainer],
        steps=100,
        mlflow_endpoint="http://10.195.1.81:5000",
    )

    ray.shutdown()


if __name__ == "__main__":
    twoWallsShortestPathAndDQN()
