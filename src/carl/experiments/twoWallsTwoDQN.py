import ray
from ray.rllib.agents.dqn import DQNTrainer

from carl.envs import twoWalls
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

# This is here to avoid the linter complaining about unused imports
# Environments are registered in the envs and only their string identifier is necessary to use them thereafter
twoWalls

# see https://github.com/ray-project/ray/blob/master/rllib/examples/
# multi_agent_two_trainers.py for reference


def trainer_setup():
    # Setup for environment #4 from evaluation
    # https://gitlab.lrz.de/kddone/kddone/uploads/478074278ab62a16133d022bb56c56a0/env4.png
    env_4_config = {
        "num_agents": 2,
        "grid_size": 7,
        "start_pos": [(1, 3), (5, 3)],
        "goal_pos": [(5, 5), (1, 1)],
        "view_size": [3, 3],
        "rel_other_agents": True,
        "encoded_observation": [True, True],
    }
    # Setup for environment #2 with different goal positions
    # https://gitlab.lrz.de/kddone/kddone/uploads/38078ccd5ff838be9c0d9043060c888d/env2.png
    env_2_config = env_4_config
    env_2_config["goal_pos"] = [(5, 3), (1, 3)]

    dqn_trainer = DQNTrainer(
        env="twoWallsEnv",
        config={
            "env_config": env_2_config,  # Adjust this to test either environment setup
            "evaluation_interval": 100,
            "evaluation_num_episodes": 10,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": True,
            "callbacks": CarlCallbacks,
        },
    )

    return dqn_trainer


def twoWallsTwoDQN():
    ray.init(ignore_reinit_error=True)

    dqn_trainer = trainer_setup()

    train(
        "TwoWallsTwoDQN",
        dqn_trainer,
        steps=1000,
        with_logging=False,
        mlflow_endpoint="http://10.195.1.81:5000",
    )

    ray.shutdown()


if __name__ == "__main__":
    twoWallsTwoDQN()
