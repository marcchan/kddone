import ray
from ray.rllib.agents.dqn import DQNTrainer

from carl.envs import oneWall
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

oneWall


# see https://github.com/ray-project/ray/blob/master/rllib/examples/
# multi_agent_two_trainers.py for reference


def trainer_setup():
    env_config = {
        "num_agents": 2,
        "width": 7,
        "height": 5,
        "start_pos": [(1, 3), (5, 3)],
        "goal_pos": [(5, 1), (1, 1)],
        "view_size": [3, 3],
        "encoded_observation": [True, True],
        "rel_other_agents": True,
    }

    dqn_trainer = DQNTrainer(
        env="OneWallEnv",
        config={
            "evaluation_interval": 5,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": False,
            "hiddens": 128,
            "callbacks": CarlCallbacks,
            "env_config": env_config,
        },
    )

    return dqn_trainer


def oneWallTwoDQN():
    ray.init(ignore_reinit_error=True)

    dqn_trainer = trainer_setup()

    train(
        "OneWallTwoDQN",
        dqn_trainer,
        steps=100,
        mlflow_endpoint="http://10.195.1.81:5000",
    )

    ray.shutdown()


if __name__ == "__main__":
    oneWallTwoDQN()
