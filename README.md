# KDDone

Group project for the 2020 LMU Big Data Science Lab.

## Running Carl Experiments

The Experiment Definitions are placed in ```src/carl/experiment```.

To run an experiment execute it as python script. E.g.:

```bash
python3 src/carl/experiments/twoWallsShortestPath.py
```

An experiment consists of:

 * Environment
 * Environment Configuration
 * Agent Setup
 * Training
 
### Environments and Environment Configuration

The different environments in our project are located in ```src/carl/envs```.
All environments are derived from ```CarlEnv```, which provides all of the necessary functionality and behaviour 
(observations, rewards, action handling, ...).

Large Parts of the CarlEnv reuse implementations from [MiniGrid](https://github.com/maximecb/gym-minigrid).

Generally, derivates of CarlEnv only provide an implementation of ```gen_grid```
which defines the placement of obstacles and other objects in the grid environment.

The ```MultiEnv``` located in ```src/carl/envs/minigrid_envs``` is a special case.
It allows to use the 
[single agent environments included in MiniGrid](https://github.com/maximecb/gym-minigrid#included-environments) 
with multiple agents.

Environments allow the configuration of multiple parameters like:
 * size of the environment
 * placement of agent start positions and goals
 * values for rewards
 * range of sight
 * visibility/encoding of grid objects in the observation
 
For a full list of available configuration parameters and default values see
```src/carl/envs/GLOBAL_VAR.py```

Default values e.g. for start and goal positions may be overwritten by some environments, e.g. if obstacles are placed
randomly and static positions are likely to result in unreachable targets.

### Agent Setup

Agent policies are trained using Ray RLlib.

Minimal example for a DQN Agent: 
```python
dqn_trainer = DQNTrainer(
    env="twoWallsEnv",
    config={
        "env_config": env_config,
    }
)
```

Example including rendering of evaluation videos and logging of step rewards:
```python
dqn_trainer = DQNTrainer(
    env="twoWallsEnv",
    config={
        "env_config": env_config,
        "evaluation_interval": 1,
        "evaluation_num_episodes": 1,
        "evaluation_config": {"explore": False},
        "num_workers": 1,
        "use_pytorch": True,
        "callbacks": CarlCallbacks,
    }
)
```

### Training

Helpers for training policies are contained in ```src/carl/training/```
They support the logging of training results to mlflow by setting up the connection to the mlflow endpoint 
(e.g. localhost:5000, see Experiment Tracking below) and extracting/formatting of relevant information from the training
results.

## Developer Setup

### Installation

To install all development dependencies and the project (including project 
dependencies) in [editable mode](https://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode),
 run:

```
pip3 install -r requirements.txt
```

### Testing
To run all tests, run:

```
tox
```

Some test take a long time to run. Those should be marked by ```@pytest.mark.slow```
For skipping these tests use following command:

```
tox -- -m "not slow"
```

It might be necessary to delete the ``.tox`` directory when adding new dependencies.


### Experiment Tracking (ML Flow)
One way to access the ML Flow Server is ssh port forwarding.
This also works for tracking local runs of the experiments.


~/.ssh/config (also requires setup of CIP ssh config)
```
Host kddone_tooling
 Hostname 10.195.1.81
 User ubuntu
 ProxyJump CIP
 IdentityFile ~/.ssh/id_ecdsa_kddone
```

Then run
```bash
 ssh -L 5000:localhost:5000 kddone_tooling
```


## Dependencies
