import ray
from ray.tune import register_env

from carl.agents.DijkstraPolicy import DijkstraTrainer
from carl.envs.empty import MultiEmptyEnv
from carl.training import train
from carl.training.custom_metrics import CarlCallbacks

ray.init(ignore_reinit_error=True)

env_config = {
    "num_agents": 2,
    "grid_size": 8,
    "start_pos": [(1, 1), (2, 2)],
    "view_size": [16, 16],
    "encoded_observation": [True, True],
}

register_env("multiGridEmpty", lambda config: MultiEmptyEnv(env_config))

trainer = DijkstraTrainer(
    env="multiGridEmpty",
    config={
        "num_workers": 1,
        "use_pytorch": False,
        "env_config": env_config,
        "callbacks": CarlCallbacks,
    },
)

train("ShortestPathAgent", trainer, steps=1)

ray.shutdown()
