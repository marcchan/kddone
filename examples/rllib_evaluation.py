from carl.envs.empty import MultiEmptyEnv
import ray
from ray.rllib.agents.dqn import DQNTrainer
from ray.tune import register_env

from carl.training.custom_metrics import CarlCallbacks

test_envs = [
    # MultiGridEmpty,
    MultiEmptyEnv,
]


ray.init(ignore_reinit_error=True)
for Env in test_envs:
    register_env("MultiEmptyEnv", lambda config: Env(config))

    trainer = DQNTrainer(
        env="MultiEmptyEnv",
        config={
            "log_level": "INFO",
            "evaluation_interval": 1,
            "evaluation_num_episodes": 1,
            "evaluation_config": {"explore": False},
            "num_workers": 1,
            "use_pytorch": True,
            "callbacks": CarlCallbacks,
            "env_config": {
                "num_agents": 1,
                # "start_pos": [
                #     (1,1),
                #     (2,2),
                # ],
            },
        },
    )

    for train_iter in range(1):
        result = trainer.train()

        ray.logger.info(f"Training Iteration {train_iter} result: {result}")
        ray.logger.info(f"Evaluation Metrics {result['evaluation']}")

ray.shutdown()
