import ray
from gym_minigrid.envs.multiroom import MultiRoomEnv
from gym_minigrid.wrappers import FullyObsWrapper
from ray.rllib.agents.ppo import PPOTrainer
from ray.tune.registry import register_env

from carl.envs.minigrid_envs import make_multiagent

"""
Example usage of a custom environment via the RLlib MultiAgentEnv interface
https://docs.ray.io/en/latest/rllib-env.html

class MyEnv(gym.Env):
    def __init__(self, env_config):
        self.action_space = <gym.Space>
        self.observation_space = <gym.Space>
    def reset(self):
        return <obs>
    def step(self, action):
        return <obs>, <reward: float>, <done: bool>, <info: dict>

Minigrid should also return similar values to this, if it doesn't already
"""


# As per the warning, we have to register new envs to the Ray registry
# https://docs.ray.io/en/latest/rllib-env.html#configuring-environments
def env_creator(env_config):
    params_mr = env_config["mr_params"]
    mr_env = MultiRoomEnv(*params_mr)
    rllib_mr_env = FullyObsWrapper(mr_env)
    return rllib_mr_env  # return an env instance


register_env("my_mr_env", env_creator)
make_multiagent("my_mr_env")


# Observations can be filtered, e.g. MeanStd
# See https://github.com/ray-project/ray/blob/master/rllib/agents/ppo/ppo.py
# custom_cfg = DEFAULT_CONFIG
# custom_cfg["observation_filter"] = "MeanStdFilter"

ray.init()
# Use RLlib multi-agent example for MultiAgentEnv interface implementation
trainer = PPOTrainer(
    env="my_mr_env",
    config={
        "num_workers": 1,
        "use_pytorch": True,
        "env_config": {"num_agents": 4, "mr_params": (2, 5)}
        # minNumRooms,  maxNumRooms, [maxRoomSize=10]
    },
)

while True:
    print(trainer.train())
