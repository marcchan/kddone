import ray
from ray.rllib.agents.ppo import PPOTrainer
from ray.tune import register_env

from carl.envs.minigrid_envs import MultiGridEmpty
from carl.training import train

register_env("multiGridEmpty", lambda config: MultiGridEmpty(config))

ray.init()

trainer = PPOTrainer(
    env="multiGridEmpty",
    config={
        "num_workers": 1,
        "use_pytorch": True,
        "env_config": {
            "num_agents": 2,
            # "start_pos": [
            #     (1,1),
            #     (2,2),
            # ],
        },
    },
)

train("PPOTrainer", trainer)
