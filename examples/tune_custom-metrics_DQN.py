#!/usr/bin/env python
"""Example of using PBT with RLlib.
Note that this requires a cluster with at least 8 GPUs in order for all trials
to run concurrently, otherwise PBT will round-robin train the trials which
is less efficient (or you can set {"gpu": 0} to use CPUs for SGD instead).
Note that Tune in general does not need 8 GPUs, and this is just a more
computationally demanding example.
"""
import ray
import random

from ray.rllib.agents.dqn import DQNTrainer
from ray.tune.schedulers import PopulationBasedTraining
from ray.tune import run, sample_from, register_env

from ray.rllib.agents.dqn import DEFAULT_CONFIG

from carl.training import train
from carl.envs.empty import MultiEmptyEnv
from carl.training.custom_metrics import CarlCallbacks

# Modify DQN default_config to our needs
# See https://github.com/ray-project/ray/blob/67c01455fe313a65cc65640e52892ed91e08ea8b/rllib/agents/dqn/dqn.py#L22
# for options for policy config
CARL_DQN_CONFIG = DEFAULT_CONFIG
# This is the amount of training steps for each Trainer.train() method execution
CARL_DQN_CONFIG["timesteps_per_iteration"] = 1000
# Prevent iterations from going lower than this time span: 1s
CARL_DQN_CONFIG["min_iter_time_s"] = 1


ray.init(
    ignore_reinit_error=True,
    # memory=2000000000,  # tune_memory: 0.2e10 == 2GB
    # num_cpus=2,
    # object_store_memory=500000000,
    # redis_max_memory=500000000,
)


def create_env(env_config: dict):
    env = MultiEmptyEnv(env_config)
    env.reset()
    env.grid.horz_wall(3, 2, 1)
    return env


register_env("multiGridEmpty", lambda env_config: create_env(env_config))


def trainable(config):
    epochs = config["epochs"]
    experiment_name = config["experiment_name"]
    del config["experiment_name"]
    del config["epochs"]
    tune_params = config["tune_params"]
    del config["tune_params"]
    # merge and keep config (default with defined parameters)
    config = {**CARL_DQN_CONFIG, **config}

    trainer = DQNTrainer(env=config["env"], config=config)
    train(
        experiment_name,
        trainer,
        steps=epochs,
        with_logging=True,
        tune_params=tune_params,
        mlflow_endpoint="http://10.195.1.81:5000",
    )
    # checkpoint = trainer.save()
    # print(checkpoint)


if __name__ == "__main__":
    pbt = PopulationBasedTraining(
        time_attr="time_total_s",
        metric="episode_reward_mean",
        mode="max",
        perturbation_interval=120,
        resample_probability=0.25,
        # Specifies the mutations of these hyperparams
        hyperparam_mutations={
            "lr": [1e-3, 5e-4, 1e-4, 5e-5, 1e-5],
            "double_q": [True, False],
            "train_batch_size": lambda: random.randint(32, 1024),
        },
    )

    run(
        trainable,
        name="pbt_dqn_test",
        scheduler=pbt,
        num_samples=1,
        config={
            "experiment_name": "Single agent Q-Learning",
            "epochs": 100,
            "env": "multiGridEmpty",
            "num_workers": 1,
            "env_config": {
                "num_agents": 1,
                "grid_size": 7,
                "start_pos": [(1, 3)],
                "goal_pos": [(5, 3)],
                "view_size": [3],
                "encoded_observation": [True],
            },
            # "log_level": "DEBUG",
            "use_pytorch": True,
            # These params are tuned from a fixed starting value.
            "lr": 5e-4,
            #  These params start off randomly drawn from a set.
            "train_batch_size": sample_from(
                lambda spec: random.choice([32, 64, 128, 256, 512, 1024])
            ),
            "tune_params": list(
                pbt._hyperparam_mutations.keys()
            ),  # for logging hyperparam
            "double_q": False,
            "callbacks": CarlCallbacks,
        },
    )
