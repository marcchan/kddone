from carl.envs.empty import MultiEmptyEnv

env_config = {
    "num_agents": 2,
    "grid_size": 7,
    "start_pos": [(1, 3), (5, 3)],
    "goal_pos": [(5, 1), (1, 1)],
    "view_size": [4, 4],
    "encoded_observation": [True, True],
    "rel_other_agents": True,
}

env = MultiEmptyEnv(env_config)
env.reset()
env.grid.horz_wall(2, 2, 3)
env.grid.horz_wall(1, 4, 5)
env.grid.horz_wall(1, 5, 5)
print(env)
