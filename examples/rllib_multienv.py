import ray
from ray.rllib.agents.ppo import PPOTrainer

from carl.env.multi_agent_rllib import make_multiagent

# https://docs.ray.io/en/latest/rllib-env.html

ray.init()
# Use RLlib multi-agent example for MultiAgentEnv interface implementation
trainer = PPOTrainer(
    env=make_multiagent("CartPole-v0"),
    config={"use_pytorch": True, "num_workers": 10, "env_config": {"num_agents": 10}},
)

while True:
    print(trainer.train())
