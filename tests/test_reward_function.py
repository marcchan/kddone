from gym_minigrid.minigrid import Goal

from carl.envs import CarlEnv
from carl.envs.GLOBAL_VAR import (
    ACT_TO_VEC,
    DEFAULT_CONFIG,
)
from carl.envs.agent_goal import AgentGoal
from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiGridEmpty

test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


def test_step_reward():
    config = {"num_agents": 1}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.east})
        assert reward[0] == DEFAULT_CONFIG["step_reward"]


def test_step_reward_config():
    config = {"num_agents": 1, "step_reward": -10}

    for Env in test_envs:
        multi_grid = Env(config)
        multi_grid.reset()
        obs, reward, done, info = multi_grid.step({0: multi_grid.Actions.east})
        assert reward[0] == -10


def test_wait_on_goal():
    config = {"num_agents": 1, "goal_reward": 10}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        agent_pos = multiMiniGrid.agents[0].agent_pos
        # Set a goal on top of the agent
        multiMiniGrid.put_obj(Goal(), agent_pos[0], agent_pos[1])
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.wait})
        assert reward[0] == config["goal_reward"]
        assert done[0] is True
        assert done["__all__"] is True


def test_move_on_goal():
    config = {"num_agents": 1, "goal_reward": 10}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        agent_pos = multiMiniGrid.agents[0].agent_pos
        # Set a goal to the south of the agent
        goal_pos = agent_pos + ACT_TO_VEC[multiMiniGrid.Actions.south]
        multiMiniGrid.put_obj(Goal(), *goal_pos)
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.south})
        assert reward[0] == config["goal_reward"]
        assert done[0] is True
        assert done["__all__"] is True


def test_wait_on_irrelevant_agent_goal():
    config = {"num_agents": 1, "goal_reward": 10}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        agent_pos = multiMiniGrid.agents[0].agent_pos
        # Set a goal on top of the agent
        multiMiniGrid.put_obj(AgentGoal([]), agent_pos[0], agent_pos[1])
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.wait})
        assert reward[0] == DEFAULT_CONFIG["step_reward"]
        assert done[0] is False


def test_move_on_irrelevant_agent_goal():
    config = {"num_agents": 1, "goal_reward": 10}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        agent_pos = multiMiniGrid.agents[0].agent_pos
        # Set a goal to the south of the agent
        goal_pos = agent_pos + ACT_TO_VEC[multiMiniGrid.Actions.south]
        multiMiniGrid.put_obj(AgentGoal([]), *goal_pos)
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.south})
        assert reward[0] == DEFAULT_CONFIG["step_reward"]
        assert done[0] is False


def test_wait_on_relevant_agent_goal():
    config = {"num_agents": 1, "goal_reward": 10, "goal_pos": [(1, 1)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.wait})
        assert reward[0] == 10
        assert done[0] is True


def test_move_on_relevant_agent_goal():
    config = {"num_agents": 1, "goal_reward": 10}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        agent_pos = multiMiniGrid.agents[0].agent_pos
        # Set a goal to the south of the agent
        goal_pos = agent_pos + ACT_TO_VEC[multiMiniGrid.Actions.south]
        multiMiniGrid.put_obj(AgentGoal([multiMiniGrid.agents[0].agent_id]), *goal_pos)
        obs, reward, done, info = multiMiniGrid.step({0: multiMiniGrid.Actions.south})
        assert reward[0] == 10
        assert done[0] is True


def test_samecell_reward():
    config = {"num_agents": 2, "start_pos": [(1, 2), (2, 2)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step(
            {0: multiMiniGrid.Actions.wait, 1: multiMiniGrid.Actions.west}
        )
        print(multiMiniGrid)
        assert (
            reward[0]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )
        assert (
            reward[1]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )


def test_crossing_samecell_reward():
    config = {"num_agents": 2, "start_pos": [(1, 2), (1, 3)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Agent crosses without staying in the same cell
        obs, reward, done, info = multiMiniGrid.step(
            {0: CarlEnv.Actions.south, 1: CarlEnv.Actions.north}
        )
        print(multiMiniGrid)
        assert (
            reward[0]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )
        assert (
            reward[1]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )


def test_samecell_reward_waiting():
    config = {"num_agents": 2, "start_pos": [(1, 2), (1, 2)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step(
            {0: CarlEnv.Actions.wait, 1: CarlEnv.Actions.wait}
        )
        print(multiMiniGrid)
        assert (
            reward[0]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )
        assert (
            reward[1]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )


def test_samecell_reward_noaction():
    config = {"num_agents": 2, "start_pos": [(1, 2), (1, 2)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step({})
        print(multiMiniGrid)
        assert len(reward.keys()) == 0


def test_samecell_reward_noaction_on_goal():
    config = {
        "num_agents": 2,
        "start_pos": [(1, 2), (1, 2)],
        "goal_pos": [(1, 2), (1, 2)],
    }

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step({})
        print(multiMiniGrid)
        assert len(reward.keys()) == 0


def test_samecell_reward_waiting_on_goal():
    config = {
        "num_agents": 2,
        "start_pos": [(1, 2), (1, 2)],
        "goal_pos": [(1, 2), (1, 2)],
    }

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step(
            {0: CarlEnv.Actions.wait, 1: CarlEnv.Actions.wait}
        )
        print(multiMiniGrid)
        assert (
            reward[0]
            == DEFAULT_CONFIG["goal_reward"] + DEFAULT_CONFIG["samecell_reward"]
        )
        assert (
            reward[1]
            == DEFAULT_CONFIG["goal_reward"] + DEFAULT_CONFIG["samecell_reward"]
        )


def test_samecell_reward_both_stepping():
    config = {"num_agents": 2, "start_pos": [(1, 2), (3, 2)]}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        print(multiMiniGrid)
        # Second agent runs into first one
        obs, reward, done, info = multiMiniGrid.step(
            {0: multiMiniGrid.Actions.east, 1: multiMiniGrid.Actions.west}
        )
        print(multiMiniGrid)
        assert (
            reward[0]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )
        assert (
            reward[1]
            == DEFAULT_CONFIG["samecell_reward"] + DEFAULT_CONFIG["step_reward"]
        )
