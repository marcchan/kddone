import numpy as np

# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiGridEmpty

test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


def test_NESW():
    """
    Test N/E/S/W direction, if the agent can reach the goal.
    """
    config = {
        "num_agents": 1,
        "grid_size": 6,
        "start_pos": [(1, 1)],
        "goal_pos": [(4, 4)],
        "view_size": [3],
    }
    # agent_id 0 => move east
    east = {0: MultiEmptyEnv.Actions.east}
    south = {0: MultiEmptyEnv.Actions.south}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        multiMiniGrid.step(east)
        multiMiniGrid.step(south)
        # Agent @ (2, 2)
        multiMiniGrid.step(east)
        multiMiniGrid.step(south)
        # Agent @ (3, 3)
        multiMiniGrid.step(east)
        _, _, done, _ = multiMiniGrid.step(south)
        assert done


def test_wait():
    """
    Test action `wait`, the agent should keep its position.
    """
    config = {
        "num_agents": 1,
        "grid_size": 6,
        "start_pos": [(1, 1)],
        "goal_pos": [(4, 4)],
        "view_size": [3],
    }

    # agent_id 0 => wait
    wait = {0: MultiEmptyEnv.Actions.wait}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        for i in range(5):
            _, _, _, _ = multiMiniGrid.step(wait)

        assert np.all(
            multiMiniGrid.agents[0].agent_pos == multiMiniGrid.agents[0].agent_start_pos
        )


def test_walls():
    """
    Let the agent walk against the wall, and test if the agent stay in the same position.
    """
    config = {
        "num_agents": 1,
        "grid_size": 6,
        "start_pos": [(1, 1)],
        "goal_pos": [(4, 4)],
        "view_size": [3],
    }

    # agent_id 0 => move north
    north = {0: MultiEmptyEnv.Actions.north}
    west = {0: MultiEmptyEnv.Actions.west}

    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        for i in range(5):
            _, _, _, _ = multiMiniGrid.step(north)
            _, _, _, _ = multiMiniGrid.step(west)

        assert np.all(
            multiMiniGrid.agents[0].agent_pos == multiMiniGrid.agents[0].agent_start_pos
        )
