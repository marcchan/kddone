import pytest
import ray

# https://github.com/ray-project/ray/issues/5636
from ray.tune import register_env
from carl.agents.DijkstraPolicy import DijkstraTrainer

# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiGridEmpty

test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


@pytest.mark.slow
def test_ray__singlemulti_custom_policy():
    ray.init(ignore_reinit_error=True)
    env_config = {
        "num_agents": 1,
        "grid_size": 8,
        "start_pos": [(1, 2)],
        "view_size": [3],
        "encoded_observation": [True],
    }
    for Env in test_envs[:]:
        register_env("multiGridEmpty", lambda config: Env(env_config))

        trainer = DijkstraTrainer(
            env="multiGridEmpty",
            config={"num_workers": 1, "use_pytorch": False, "env_config": env_config},
        )

        trainer.train()

    ray.shutdown()


@pytest.mark.slow
def test_ray_multiagent_custom_policy():
    ray.init(ignore_reinit_error=True)
    env_config = {
        "num_agents": 2,
        "grid_size": 8,
        "start_pos": [(1, 2), (2, 2)],
        "view_size": [3, 3],
        "encoded_observation": [True, True],
    }
    for Env in test_envs[:]:
        register_env("multiGridEmpty", lambda config: Env(env_config))
        trainer = DijkstraTrainer(
            env="multiGridEmpty",
            config={"num_workers": 1, "use_pytorch": False, "env_config": env_config},
        )

        trainer.train()

    ray.shutdown()
