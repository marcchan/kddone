# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv
from gym import spaces
import carl.envs.GLOBAL_VAR as GLOBAL_VAR

test_envs = [MultiEmptyEnv]


def test_render_img():
    config = {
        "num_agents": 3,
        "grid_size": 10,
        "start_pos": [(1, 1)] * 3,
        "goal_pos": [(4, 4)] * 3,
        "view_size": [3] * 3,
    }
    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        action_dict = {}
        for j, agent in zip(range(len(multiMiniGrid.agents)), multiMiniGrid.agents):
            action = spaces.Discrete(5).sample()
            action_dict[j] = action
        multiMiniGrid.step(action_dict)
        img = multiMiniGrid.render()
        assert not multiMiniGrid.window.closed
        assert img.shape == (
            multiMiniGrid.grid_size * GLOBAL_VAR.TILE_PIXELS,
            multiMiniGrid.grid_size * GLOBAL_VAR.TILE_PIXELS,
            3,
        )


def test_agent_view():
    config = {
        "num_agents": 3,
        "grid_size": 10,
        "start_pos": [(1, 1)] * 3,
        "goal_pos": [(4, 4)] * 3,
        "view_size": [3] * 3,
    }
    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()
        for agent in multiMiniGrid.agents:
            topX, topY, botX, botY = multiMiniGrid.agent_view(agent)
            assert topX >= 0 and topY >= 0 and botX >= 0 and botY >= 0
            assert (
                topX < multiMiniGrid.width
                and topY < multiMiniGrid.height
                and botX < multiMiniGrid.width
                and botY < multiMiniGrid.height
            )
