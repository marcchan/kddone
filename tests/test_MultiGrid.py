import pytest
import ray
from ray.rllib.agents.ppo import PPOTrainer

# https://github.com/ray-project/ray/issues/5636
from ray.tune import register_env

# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiDoorKey, MultiGridEmpty

from gym_minigrid.minigrid import MiniGridEnv

test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


def test_agent_startpos_no_config():
    """
    Make sure agent_pos comparison still works when startpos is not configured.
    The type of agent_pos changes from tuple to np.array when updating the
    position after Action.forward in minigrid
    """
    config = {"num_agents": 2}
    for Env in test_envs:
        print(Env)
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        assert all(multiMiniGrid.agents[0].agent_pos == (1, 1))
        assert all(multiMiniGrid.agents[1].agent_pos == (1, 1))


def test_agent_startpos_config():
    config = {"num_agents": 2, "start_pos": [(1, 2), (2, 2)]}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        assert all(multiMiniGrid.agents[0].agent_pos == (1, 2))
        assert all(multiMiniGrid.agents[1].agent_pos == (2, 2))

    multiMiniGrid = MultiDoorKey(config)
    multiMiniGrid.reset()

    assert all(multiMiniGrid.agents[0].agent_pos == (1, 2))
    assert all(multiMiniGrid.agents[1].agent_pos == (2, 2))


def test_agent_view_size_config():
    config = {"num_agents": 2, "start_pos": [(1, 2), (2, 2)], "view_size": [4, 3]}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        multiMiniGrid.reset()

        if isinstance(multiMiniGrid.agents[0], MiniGridEnv):
            assert multiMiniGrid.agents[0].agent_view_size == 7
            assert multiMiniGrid.agents[1].agent_view_size == 7
        else:
            assert multiMiniGrid.agents[0].agent_view_size == 4
            assert multiMiniGrid.agents[1].agent_view_size == 3


def test_other_agent_obs_single_agent():
    """
    If there is only one agent, there are no other agent positions.
    In this case the other_agents list should not be contained in the obs
    """
    config = {"num_agents": 1, "other_agents": True}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "other_agents" not in obs[0]


def test_other_agent_obs_multi_agent():
    config = {"num_agents": 2, "other_agents": True}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "other_agents" in obs[0]
        assert "other_agents" in obs[1]
        assert len(obs[0]["other_agents"]) == 1
        assert len(obs[1]["other_agents"]) == 1
        assert all(obs[0]["other_agents"][0] == multiMiniGrid.agents[1].agent_pos)
        assert all(obs[1]["other_agents"][0] == multiMiniGrid.agents[0].agent_pos)


def test_no_other_agent_obs():
    config = {"num_agents": 1, "other_agents": False}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "other_agents" not in obs[0]


def test_other_agent_obs_not_set():
    config = {"num_agents": 2}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "other_agents" in obs[0]


def test_rel_other_agent_obs():
    config = {"num_agents": 2, "rel_other_agents": True}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "rel_other_agents" in obs[0]
        assert all(obs[0]["rel_other_agents"][0] == (0, 0))
        assert all(obs[1]["rel_other_agents"][0] == (0, 0))


def test_rel_other_agent_obs2():
    config = {"num_agents": 2, "rel_other_agents": True, "start_pos": [(1, 1), (3, 6)]}
    for Env in test_envs:
        multiMiniGrid = Env(config)
        obs = multiMiniGrid.reset()

        assert "rel_other_agents" in obs[0]
        assert all(obs[0]["rel_other_agents"][0] == (2, 5))
        assert all(obs[1]["rel_other_agents"][0] == (-2, -5))


@pytest.mark.slow
def test_ray_training_abs():
    ray.init(ignore_reinit_error=True)
    for Env in test_envs:
        register_env("MultiEmptyEnv", lambda config: Env(config))

        trainer = PPOTrainer(
            env="MultiEmptyEnv",
            config={
                "num_workers": 1,
                "use_pytorch": True,
                "env_config": {
                    "num_agents": 2,
                    "rel_other_agents": False,
                    "start_pos": [(1, 1), (2, 2)],
                },
            },
        )

        trainer.train()
    ray.shutdown()


@pytest.mark.slow
def test_ray_training_rel():
    ray.init(ignore_reinit_error=True)
    for Env in test_envs:
        register_env("MultiEmptyEnv", lambda config: Env(config))

        trainer = PPOTrainer(
            env="MultiEmptyEnv",
            config={
                "num_workers": 1,
                "use_pytorch": True,
                "env_config": {
                    "num_agents": 2,
                    "rel_other_agents": True,
                    "start_pos": [(1, 1), (2, 2)],
                },
            },
        )

        trainer.train()
    ray.shutdown()
