# defines the Environments to test
import pytest
from tests.logging_output_DQN_dijkstra import logging_output

from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiGridEmpty
from carl.training.custom_metrics import CarlCallbacks
from carl.training import (
    flatten_metrics,
    get_policy_metric_keys,
    flatten_policy_metrics,
)

# https://github.com/ray-project/ray/issues/5636
import ray
import numpy as np
from ray.tune import register_env
from carl.agents.DijkstraPolicy import DijkstraTrainer

test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


def test_policy_multipolicy_flattening():

    flattened = flatten_metrics(logging_output)

    assert "policy_reward_min_dijkstra_policy" in flattened
    assert "policy_reward_min_dqn_policy" in flattened


def test_get_policy_metrics():
    policy_metric_keys = get_policy_metric_keys(logging_output)

    assert "policy_reward_min" in policy_metric_keys
    assert "episode_reward_min" not in policy_metric_keys


def test_flatten_policy_metrics():
    flattened = flatten_policy_metrics(logging_output)

    assert "policy_reward_min_dijkstra_policy" in flattened


@pytest.mark.slow
def test_custom_metrics_singlemulti():
    ray.init(ignore_reinit_error=True)
    env_config = {
        "num_agents": 1,
        "grid_size": 8,
        "start_pos": [(1, 2)],
        "view_size": [16],
        "encoded_observation": [True],
    }
    for Env in test_envs[:]:
        register_env("multiGridEmpty", lambda config: Env(env_config))

        trainer = DijkstraTrainer(
            env="multiGridEmpty",
            config={
                "num_workers": 1,
                "use_pytorch": False,
                "env_config": env_config,
                "callbacks": CarlCallbacks,
            },
        )

        result = trainer.train()
        custom_metrics = result["custom_metrics"]
        print(custom_metrics)

        assert custom_metrics != {}
        assert "ep_rewards_per_agent_0_mean" in custom_metrics
        assert "ep_rewards_per_agent_0_min" in custom_metrics
        assert "ep_rewards_per_agent_0_max" in custom_metrics
        assert (
            custom_metrics["ep_rewards_per_agent_0_max"] == result["episode_reward_max"]
        )

    ray.shutdown()


@pytest.mark.slow
def test_custom_metrics_multiagent():
    ray.init(ignore_reinit_error=True)
    env_config = {
        "num_agents": 2,
        "grid_size": 8,
        "start_pos": [(1, 2), (2, 2)],
        "view_size": [16, 16],
        "encoded_observation": [True, True],
    }
    for Env in test_envs[:]:
        register_env("multiGridEmpty", lambda config: Env(env_config))
        trainer = DijkstraTrainer(
            env="multiGridEmpty",
            config={
                "num_workers": 1,
                "use_pytorch": False,
                "env_config": env_config,
                "callbacks": CarlCallbacks,
            },
        )

        result = trainer.train()
        custom_metrics = result["custom_metrics"]
        print(custom_metrics)

        assert custom_metrics != {}
        rewards_per_agents = []
        for index in range(env_config["num_agents"]):
            assert "ep_rewards_per_agent_{}_mean".format(str(index)) in custom_metrics
            assert "ep_rewards_per_agent_{}_min".format(str(index)) in custom_metrics
            assert "ep_rewards_per_agent_{}_max".format(str(index)) in custom_metrics
            rewards_per_agents.append(
                custom_metrics["ep_rewards_per_agent_{}_max".format(str(index))]
            )

        assert np.isclose(sum(rewards_per_agents), result["episode_reward_max"])

    ray.shutdown()
