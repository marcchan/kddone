from carl.envs import AgentState


def test_get_rel_position():
    agent = AgentState(0, agent_start_pos=(1, 1), agent_view_size=7)

    test_pos1 = (1, 1)
    assert agent.get_rel_position(*test_pos1) == (0, 0)

    topx, topy, bottomx, bottomy = agent.get_view_exts()
    agent_view = agent.agent_view_size
    test_pos2 = (topx, topy)
    assert agent.get_rel_position(*test_pos2) == (-agent_view, -agent_view)

    test_pos3 = (bottomx, bottomy)
    assert agent.get_rel_position(*test_pos3) == (agent_view, agent_view)

    test_pos4 = (topx, bottomy)
    assert agent.get_rel_position(*test_pos4) == (-agent_view, agent_view)

    test_pos5 = (bottomx, topy)
    assert agent.get_rel_position(*test_pos5) == (agent_view, -agent_view)
