from carl.agents.utils.Dijkstra import Graph
from gym_minigrid.envs.empty import EmptyEnv5x5
from carl.envs import CarlEnv
from carl.agents.DijkstraPolicy import DijkstraPolicy

# defines the Environments to test
from carl.envs.empty import MultiEmptyEnv
from carl.envs.minigrid_envs import MultiGridEmpty


test_envs = [
    MultiGridEmpty,
    MultiEmptyEnv,
]


def test_shortestpath():
    g = Graph()
    g.add_vertex("A", {"B": 7, "C": 8})
    g.add_vertex("B", {"A": 7, "F": 2})
    g.add_vertex("C", {"A": 8, "F": 6, "G": 4})
    g.add_vertex("D", {"F": 8})
    g.add_vertex("E", {"H": 1})
    g.add_vertex("F", {"B": 2, "C": 6, "D": 8, "G": 9, "H": 3})
    g.add_vertex("G", {"C": 4, "F": 9})
    g.add_vertex("H", {"E": 1, "F": 3})

    assert g.shortest_path("A", "H") == ["H", "F", "B"]


def test_grid_to_graph_conversion():
    """
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    | >> |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |Goal|    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   """
    env = EmptyEnv5x5()
    g = Graph()
    g.from_grid(env.grid)
    # In this env, goal is always in the bottom right corner
    # Agent starts at top left
    # repr gives us the string representation for keys used by the Dijkstra algo dict.
    start = repr(env.agent_start_pos)
    goal = repr((3, 3))
    total_distance = 0
    shortest_path = list(g.shortest_path(start, goal))
    shortest_path.reverse()
    for vertex in shortest_path:
        total_distance += g.vertices[start][vertex]
        # Update start to be current vertex
        start = vertex

    assert total_distance == 4


def test_unreachable_finish():
    """
      +------------------------+
      |    |    |    |    |    |
      |    |    |    |    |    |
      |    |    |    |    |    |
      +------------------------+
      |    | >> |    |    |    |
      |    |    |    |    |    |
      |    |    |    |    |    |
      +------------------------+
      |    |    |    |    |    |
      |    |    |    |    |    |
      |    |    |    |    |    |
      +------------------------+
      |    |    |    |    |    |
      |    |    |    |    |    |
      |    |    |    |    |    |
      +------------------------+
      |    |    |    |    |    |
      |    |    |    |    |    |
      |    |    |    |    |    |                            Goal :(
      +------------------------+
      """
    env = EmptyEnv5x5()
    g = Graph()
    g.from_grid(env.grid)
    # In this env, goal is always in the bottom right corner
    # Agent starts at top left
    # repr gives us the string representation for keys used by the Dijkstra algo dict.
    start = repr(env.agent_start_pos)
    goal = repr((8, 8))
    shortest_path = g.shortest_path(start, goal)

    assert shortest_path is None


def test_dijkstra_agent():
    """
    Tests using the Custom Policy the same way we would use the Agent.policy(state) --> action interface.
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    | >> |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |Goal|    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   |    |    |    |    |    |
   |    |    |    |    |    |
   |    |    |    |    |    |
   +------------------------+
   """
    env_config = {
        "num_agents": 1,
        "grid_size": 5,
        "start_pos": [(1, 1)],
        "goal_pos": [(3, 3)],
        "view_size": [10],
        "encoded_observation": [True],
    }
    expected_steps = 4
    success = []
    dummy_env = CarlEnv(
        {}
    )  # Our action and observation space definitions don't change atm
    dijkstra_policy = DijkstraPolicy(
        obs_space=dummy_env.observation_space,
        action_space=dummy_env.action_space,
        config={},
    )
    for Env in test_envs:
        counter = 0
        episode_done = {}
        multiMiniGrid = Env(env_config)
        last_state = multiMiniGrid.reset()
        while not episode_done:
            if (
                counter > expected_steps
            ):  # We expect to reach the goal in four steps by taking the shortest path
                success.append(False)
                break
            act_dict = {}
            for agent_idx, state in last_state.items():
                action, _, _ = dijkstra_policy.compute_single_action(last_state)
                act_dict[agent_idx] = action
            next_state, _, done, _ = multiMiniGrid.step(act_dict)
            if done[0]:
                success.append(True)
            episode_done = done[0]
            last_state = next_state
            counter += 1

    assert all(success), "Successes: {}".format(success)
