#!/usr/bin/env bash

mlflow server \
    --backend-store-uri ~/MLFlow-disk \
    --default-artifact-root sftp://ubuntu@127.0.0.1:~/MLFlow-artifacts \
    --host 127.0.0.1
