#!/use/bin/env bash
# See: https://docs.docker.com/compose/
# This could be handy depending on how we model our system 
# If we want to encapsulate services into Docker containers that run concurrently, this would run them
docker-compose up
