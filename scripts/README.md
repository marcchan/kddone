# OpenStack CLI
Had a few issues with this one. When installing through `apt` on Ubuntu 16.04, the openstack CLI is one major version behind the current one and thus the tutorials on the LRZ documentation don't work.
## Installing (Ubuntu 16.04)
1. Create Python virtual environment - I'm using conda
1. Activate environment
1. `pip install python-openstackclient==3.19` for the newest minor version - partially from [Openstack docs](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html)
Note: At this point you should be able to use `openstack` from the terminal within the venv you've created. Also, the version is important, cost me half an hour to find one that actually works^^
## Usage
`openstack [command] --help` to see a list of the commands and options for specific commands.
To use in conjunction with our OpenStack account, first authenticate through the CLI:
`source di52raf-openrc.sh`
The script will prompt you to input the password for the account. After this step, all `openstack` commands can be used from the same
terminal session.
