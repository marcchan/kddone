#!/usr/bin/env bash
# run this in the same directory as the Dockerfile
# https://stackoverflow.com/questions/45141402/build-and-run-dockerfile-with-one-command
# For explanations on -i and -t, see man or run docker run --help
# /bin/bash gives us a terminal-like environment
docker run --rm -it $(docker build -q .) /bin/bash
