#!/usr/bin/env bash
# $1 is the first argument given to the script
# Usage: bash delete-vm.sh very-long-id-for-the-vm-you-want-to-delete
SERVER_ID=$1
VOLUMES=$(openstack server show $SERVER_ID -c volumes_attached -f value | cut -d "'" -f 2 | tr '\n' ' ') 
openstack server delete --wait $SERVER_ID
openstack volume delete $VOLUMES
