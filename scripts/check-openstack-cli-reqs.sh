#!/usr/bin/env bash
# || Only executes the command if the first one was falsy. 
# If there's no installed packages for openstack, it will remind you to
# install them
which openstack || echo "Please install the openstack-cli dependencies through apt or pip."
